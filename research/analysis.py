import os
import socket
from uuid import getnode as get_mac
import MySQLdb as dbapi
import pandas as pd
import graph_tool as gt
import matplotlib
from collections import defaultdict
from graph_tool.all import Graph, graph_draw, sfdp_layout

# Assign local machine identifiers
mac = get_mac()
home_path = os.environ['HOME']

# Setup MySQL credentials
def mysql_setup():
    if socket.gethostname() in ['ResearchRoom13', 'esperie-AW'] or socket.gethostbyname(socket.gethostname()).startswith('10.'):
        # return dbapi.connect(host='10.2.8.21', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)
        return dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)
    elif socket.gethostbyname(socket.gethostname()).startswith('192.'):
        return dbapi.connect(host='192.168.1.109', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)
    else:
        return dbapi.connect(host='localhost', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

con = mysql_setup()

# Setup reference folder
if mac == 203308018878304:
    dirpath = '/Users/Jack/OneDrive/Academia/Research Projects/Network Centrality/Boardex 2014/'
elif mac == 128454777953710:
    dirpath = 'D:/OneDrive/Academia/Research Projects/Network Centrality/Boardex 2014/'

con = dbapi.connect(host='10.2.16.35', user='Esperie', passwd='momoshU1979', db='boardex',
                    charset='utf8', use_unicode=True)
df = pd.read_sql("SELECT directorid, p1_ed_ned_sm, linked_directorid, p2_ed_ned_sm, start_year, end_year, connected_companyID "
                 "FROM offshelf_network_board", con)

df.rename(columns={'connected_companyID': 'companyid'}, inplace=True)

df_dir1 = df[['directorid', 'p1_ed_ned_sm', 'companyid', 'start_year', 'end_year']].drop_duplicates()
df_dir1.rename(columns={'p1_ed_ned_sm': 'ed_ned'}, inplace=True)

df_dir2 = df[['linked_directorid', 'p2_ed_ned_sm', 'companyid', 'start_year', 'end_year']].drop_duplicates()
df_dir2.rename(columns={'linked_directorid': 'directorid', 'p2_ed_ned_sm': 'ed_ned'}, inplace=True)

df_dir = pd.concat([df_dir1, df_dir2]).drop_duplicates()

co_id = pd.read_excel(dirpath + 'gvkey.xlsx')
co_id.columns = ['coname', 'companyid', 'isin', 'ticker', 'cik', 'gvkey']

df_dir = pd.merge(left=df_dir, right=co_id, how='inner', on='companyid')
df_min = df_dir[['directorid', 'companyid', 'ed_ned', 'coname', 'isin', 'ticker', 'cik', 'gvkey', 'start_year']]
df_min = df_min.groupby(['directorid', 'companyid', 'ed_ned', 'coname', 'isin', 'ticker', 'cik', 'gvkey']).min().reset_index()

df_max = df_dir[['directorid', 'companyid', 'ed_ned', 'coname', 'isin', 'ticker', 'cik', 'gvkey', 'end_year']]
df_max = df_max.groupby(['directorid', 'companyid', 'ed_ned', 'coname', 'isin', 'ticker', 'cik', 'gvkey']).max().reset_index()

df_final = pd.merge(left=df_min, right=df_max, how='left', on=['directorid', 'companyid', 'ed_ned', 'coname', 'isin', 'ticker', 'cik', 'gvkey'])

# INSERT INTO MYSQL SERVER
s1 = len(max(df_dir['coname'], key=lambda x: len(x)))  # VARCHAR()
s2 = len(max(df_dir[df_dir['isin'].notnull()]['isin'], key=lambda x: len(x)))  # VARCHAR()
s3 = len(max(df_dir[df_dir['ticker'].notnull()]['ticker'], key=lambda x: len(x)))  # VARCHAR()

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS company_tag')
    cur.execute('CREATE TABLE company_tag(directorID BIGINT, companyID BIGINT, ed_ned VARCHAR(3), '
                'coname VARCHAR(%d), isin VARCHAR(%d), ticker VARCHAR(%d), cik BIGINT, '
                'gvkey BIGINT, start_year INT, end_year INT)' % (s1, s2, s3))

    for entry in df_final.values.tolist():
        r = []
        for e in entry:
            if pd.isnull(e):
                r.append(None)
                continue
            if isinstance(e, float):
                r.append(int(e))
                continue
            if e == u'.':
                r.append(None)
                continue
            if type(e) == unicode:
                r.append(e)
                continue
            r.append(e)

        cur.execute('INSERT INTO company_tag '
                    'VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                    (r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9]))