import datetime
import pandas as pd
import MySQLdb as dbapi
from sas7bdat import SAS7BDAT

dirpath = "E:/ChinaDir/director/"
filename = "pair_new.sas7bdat"

dsiobj = SAS7BDAT(dirpath + filename)  # Create a sas7bdat object

print dsiobj.header  # Check headers

df = dsiobj.to_data_frame()  # Create pandas dataframe

df_select = df[['id', 'id_m', 'year']]  # Extract pairs and year

## Check length
# age: INT
# age_m: INT
# gender: VARCHAR(6)
# gender_m: VARCHAR(6)
# id: BIGINT
# id_m: BIGINT
# s1 = len(max(df['name'], key=lambda x: len(x)))  # VARCHAR()
# s2 = len(max(df['name_m'], key=lambda x: len(x)))  # VARCHAR()
# s3 = len(max(df['positioncategory'], key=lambda x: len(x)))
# s4 = len(max(df['positioncategory_m'], key=lambda x: len(x)))
# s5 = len(max(df['specificposition'], key=lambda x: len(x)))  # VARCHAR()
# s6 = len(max(df['specificposition_m'], key=lambda x: len(x)))  # VARCHAR()
# stockcode: INT
# year: INT

con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='china_director', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS new_pair')
    cur.execute('CREATE TABLE new_pair(id BIGINT, id_m BIGINT, year INT)')

    for entry in df_select.values.tolist():
        cur.execute('INSERT INTO new_pair VALUES(%s, %s, %s)',
                    (entry[0], entry[1], entry[2]))