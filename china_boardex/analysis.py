import os
import socket
from uuid import getnode as get_mac
from colorama import Fore
import datetime
import MySQLdb as dbapi
import pandas as pd
import graph_tool as gt
from graph_tool.all import Graph

# Assign local machine identifiers
mac = get_mac()
home_path = os.environ['HOME']

# Check if openMP is open (increases speed by a factor of 3)
gt.show_config()

# Load data from SQL
def db_setup():
    if socket.gethostname() in ['ResearchRoom13', 'esperie-AW'] or socket.gethostbyname(socket.gethostname()).startswith('10.'):
        # return dbapi.connect(host='10.2.8.21', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)
        return dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='china_director', charset='utf8', use_unicode=True)
    elif socket.gethostbyname(socket.gethostname()).startswith('192.'):
        return dbapi.connect(host='192.168.1.109', user='Esperie', passwd='momoshU1979', db='china_director', charset='utf8', use_unicode=True)
    else:
        return dbapi.connect(host='localhost', user='Esperie', passwd='momoshU1979', db='china_director', charset='utf8', use_unicode=True)

con = db_setup()
df = pd.read_sql("SELECT dcode as directorid, dcode_match as linked_directorid, year "
                 "FROM select_pair", con)

# Execute in graphtool
################################################ Current links only #############################################
years = df.year.unique().astype(int).tolist()

for y in years:
    # Set path
    if mac == 189262221327399:
        path = '/home/triojent/Dropbox/Network Centrality/China Director/current/' + str(y) + '.gt'
    elif mac == 189262223071825:
        path = '/media/esperie/Data/Dropbox/Academia/Research Projects/Network Centrality/China Director/current/' + str(y) + '.gt'
    elif mac == 105788756394591:
        path = '/home/esperie/Dropbox/Academia/Research Projects/Network Centrality/China Director/current/' + str(y) + '.gt'
    else:
        raise Exception('This is not a LINUX machine!')

    # Extract pair from dataframe
    select = df[df.year == y].loc[:, ['directorid', 'linked_directorid']].drop_duplicates().astype(int).values.tolist()

    # Set the pairs to obtain unique ids
    ids = set()
    for d1, d2 in select:
        ids.add(d1)
        ids.add(d2)
    ids = sorted(list(ids))

    # Create dictionary mapping of ids to index
    id_list = {}
    for i in xrange(len(ids)):
        id_list[ids[i]] = i

    # Create graph and initialize variables
    now = datetime.datetime.now()
    print(Fore.CYAN + "Creating graph and adding vertices now... (%s)" % now)
    g = Graph(directed=False)

    g.add_vertex(len(id_list.keys()))
    id = g.new_vertex_property('int64_t')
    for s, t in select:
        g.add_edge(g.vertex(id_list[s]), g.vertex(id_list[t]))
        id[g.vertex(id_list[s])] = s
        id[g.vertex(id_list[t])] = t
    g.vertex_properties['directorid'] = id

    time_taken = datetime.datetime.now() - now
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))
    print(Fore.BLUE + 'Year: %d.' % y)
    print(Fore.BLUE + 'Number of vertices: %d.' % g.num_vertices())
    print(Fore.BLUE + 'Number of edges: %d.\n' % g.num_edges())

    # Save graph
    now = datetime.datetime.now()
    print 'Saving graph to gt format... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))

    ## Optional: Load graph
    #g = load_graph(path)

    # Compute measures and save to graph as internal property maps
    now = datetime.datetime.now()
    print(Fore.CYAN + 'Calculating Eigenvector centrality... (%s)' % str(now))
    start_time = datetime.datetime.now()
    eigen_max, eigen_map = gt.centrality.eigenvector(g)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.' % str(time_taken))
    now = datetime.datetime.now()
    print(Fore.CYAN + 'Creating internal property map and saving graph to gt format... (%s)' % str(now))
    start_time = datetime.datetime.now()
    g.vertex_properties['eigenvector'] = eigen_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))

    now = datetime.datetime.now()
    print(Fore.CYAN + 'Calculating Betweenness centrality... (%s)' % str(now))
    start_time = datetime.datetime.now()
    vertex_between, edge_between = gt.centrality.betweenness(g)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.' % str(time_taken))
    now = datetime.datetime.now()
    print (Fore.CYAN + 'Creating internal property map and saving graph to gt format... (%s)' % str(now))
    start_time = datetime.datetime.now()
    g.vertex_properties['vertex_between'] = vertex_between
    g.edge_properties['edge_between'] = edge_between
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))

    now = datetime.datetime.now()
    print(Fore.CYAN + 'Calculating Closeness centrality... (%s)' % str(now))
    start_time = datetime.datetime.now()
    close_map = gt.centrality.closeness(g)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.' % str(time_taken))
    now = datetime.datetime.now()
    print(Fore.CYAN + 'Creating internal property map and saving graph to gt format... (%s)' % str(now))
    start_time = datetime.datetime.now()
    g.vertex_properties['closeness'] = close_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))

    now = datetime.datetime.now()
    print(Fore.CYAN + 'Calculating Hits centrality... (%s)' % str(now))
    start_time = datetime.datetime.now()
    hits_max, authority_map, hub_map = gt.centrality.hits(g)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.' % str(time_taken))
    now = datetime.datetime.now()
    print(Fore.CYAN + 'Creating internal property map and saving graph to gt format... (%s)' % str(now))
    start_time = datetime.datetime.now()
    g.vertex_properties['authority'] = authority_map
    g.vertex_properties['hub'] = hub_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))


################################# Extract into database (current centrality)

dirpath, filename = os.path.split(path)
fileslist = []
for root, dirs, files in os.walk(dirpath):
    # for dir in dirs:
    for file in files:
        if file.endswith('.gt'):
            fileslist.append((dirpath + '/' + file))

results = []

for p in fileslist:
    g = gt.load_graph(p)
    year = int(p.rsplit('/', 1)[-1].replace('.gt', ''))
    num_nodes = g.num_vertices()
    num_edges = g.num_edges()
    id = g.vertex_properties['directorid']
    eigenvector = g.vertex_properties['eigenvector']
    vertex_between = g.vertex_properties['vertex_between']
    closeness = g.vertex_properties['closeness']
    authority = g.vertex_properties['authority']
    hub = g.vertex_properties['hub']

    for v in g.vertices():
        results.append({
            'directorID': id[v],
            'eigenvector': eigenvector[v],
            'vertex_betweenness': vertex_between[v],
            'closeness': closeness[v],
            'authority': authority[v],
            'hub': hub[v],
            'degree': v.out_degree(),
            'degree_norm': float(v.out_degree())/num_nodes,
            'year': year
        })

## Send to SQL

con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='china_director', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS centrality_current')
    cur.execute('CREATE TABLE centrality_current(directorID BIGINT, year INT, degree BIGINT, degree_norm DOUBLE, eigenvector DOUBLE, betweenness DOUBLE, closeness DOUBLE, authority DOUBLE, hub DOUBLE)')

    for r in results:
        cur.execute('INSERT INTO centrality_current VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)', (r['directorID'],
        r['year'], r['degree'], r['degree_norm'], r['eigenvector'], r['vertex_betweenness'], r['closeness'],
        r['authority'], r['hub']))

############################################### Past links do not expire ########################################
years = df.year.unique().astype(int).tolist()

for y in years:
    # Set path
    if mac == 189262221327399:
        path = '/home/triojent/Dropbox/Network Centrality/China Director/past/' + str(y) + '.gt'
    elif mac == 189262223071825:
        path = '/media/esperie/Data/Dropbox/Academia/Research Projects/Network Centrality/China Director/past/' + str(y) + '.gt'
    elif mac == 105788756394591:
        path = '/home/esperie/Dropbox/Academia/Research Projects/Network Centrality/China Director/past/' + str(y) + '.gt'
    else:
        raise Exception('This is not a LINUX machine!')

    # Extract pair from dataframe
    select = df[df.year <= y].loc[:, ['directorid', 'linked_directorid']].drop_duplicates().astype(int).values.tolist()

    # Set the pairs to obtain unique ids
    ids = set()
    for d1, d2 in select:
        ids.add(d1)
        ids.add(d2)
    ids = sorted(list(ids))

    # Create dictionary mapping of ids to index
    id_list = {}
    for i in xrange(len(ids)):
        id_list[ids[i]] = i

    # Create graph and initialize variables
    now = datetime.datetime.now()
    print(Fore.CYAN + "Creating graph and adding vertices now... (%s)" % now)
    g = Graph(directed=False)

    g.add_vertex(len(id_list.keys()))
    id = g.new_vertex_property('int64_t')
    for s, t in select:
        g.add_edge(g.vertex(id_list[s]), g.vertex(id_list[t]))
        id[g.vertex(id_list[s])] = s
        id[g.vertex(id_list[t])] = t
    g.vertex_properties['directorid'] = id

    time_taken = datetime.datetime.now() - now
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))
    print(Fore.BLUE + 'Year: %d.' % y)
    print(Fore.BLUE + 'Number of vertices: %d.' % g.num_vertices())
    print(Fore.BLUE + 'Number of edges: %d.\n' % g.num_edges())

    # Save graph
    now = datetime.datetime.now()
    print 'Saving graph to gt format... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))

    ## Optional: Load graph
    #g = load_graph(path)

    # Compute measures and save to graph as internal property maps
    now = datetime.datetime.now()
    print(Fore.CYAN + 'Calculating Eigenvector centrality... (%s)' % str(now))
    start_time = datetime.datetime.now()
    eigen_max, eigen_map = gt.centrality.eigenvector(g)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.' % str(time_taken))
    now = datetime.datetime.now()
    print(Fore.CYAN + 'Creating internal property map and saving graph to gt format... (%s)' % str(now))
    start_time = datetime.datetime.now()
    g.vertex_properties['eigenvector'] = eigen_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))

    now = datetime.datetime.now()
    print(Fore.CYAN + 'Calculating Betweenness centrality... (%s)' % str(now))
    start_time = datetime.datetime.now()
    vertex_between, edge_between = gt.centrality.betweenness(g)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.' % str(time_taken))
    now = datetime.datetime.now()
    print (Fore.CYAN + 'Creating internal property map and saving graph to gt format... (%s)' % str(now))
    start_time = datetime.datetime.now()
    g.vertex_properties['vertex_between'] = vertex_between
    g.edge_properties['edge_between'] = edge_between
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))

    now = datetime.datetime.now()
    print(Fore.CYAN + 'Calculating Closeness centrality... (%s)' % str(now))
    start_time = datetime.datetime.now()
    close_map = gt.centrality.closeness(g)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.' % str(time_taken))
    now = datetime.datetime.now()
    print(Fore.CYAN + 'Creating internal property map and saving graph to gt format... (%s)' % str(now))
    start_time = datetime.datetime.now()
    g.vertex_properties['closeness'] = close_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))

    now = datetime.datetime.now()
    print(Fore.CYAN + 'Calculating Hits centrality... (%s)' % str(now))
    start_time = datetime.datetime.now()
    hits_max, authority_map, hub_map = gt.centrality.hits(g)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.' % str(time_taken))
    now = datetime.datetime.now()
    print(Fore.CYAN + 'Creating internal property map and saving graph to gt format... (%s)' % str(now))
    start_time = datetime.datetime.now()
    g.vertex_properties['authority'] = authority_map
    g.vertex_properties['hub'] = hub_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))


################################# Extract into database (past centrality)
dirpath, filename = os.path.split(path)
fileslist = []
for root, dirs, files in os.walk(dirpath):
    for dir in dirs:
        for file in files:
            if file.endswith('.gt'):
                fileslist.append((dirpath + '/' + file))

results = []

for p in fileslist:
    g = gt.load_graph(p)
    year = int(path.rsplit('/', 1)[-1].replace('.gt', ''))
    num_nodes = g.num_vertices()
    num_edges = g.num_edges()
    id = g.vertex_properties['director_id']
    eigenvector = g.vertex_properties['eigenvector']
    vertex_between = g.vertex_properties['vertex_between']
    closeness = g.vertex_properties['closeness']
    authority = g.vertex_properties['authority']
    hub = g.vertex_properties['hub']

    for v in g.vertices():
        results.append({
            'directorID': id[v],
            'eigenvector': eigenvector[v],
            'vertex_betweenness': vertex_between[v],
            'closeness': closeness[v],
            'authority': authority[v],
            'hub': hub[v],
            'degree': v.out_degree(),
            'degree_norm': float(v.out_degree())/num_nodes,
            'year': year
        })

## Send to SQL

con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='china_director', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS centrality_past')
    cur.execute('CREATE TABLE centrality_past(directorID BIGINT, year INT, degree BIGINT, degree_norm DOUBLE, eigenvector DOUBLE, betweenness DOUBLE, closeness DOUBLE, authority DOUBLE, hub DOUBLE)')

    for r in results:
        cur.execute('INSERT INTO centrality_past VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)', (r['directorID'],
        r['year'], r['degree'], r['degree_norm'], r['eigenvector'], r['vertex_betweenness'], r['closeness'],
        r['authority'], r['hub']))