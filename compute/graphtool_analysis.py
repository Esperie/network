import os
import socket
from uuid import getnode as get_mac
import MySQLdb as dbapi
import pandas as pd
import graph_tool as gt
import matplotlib
from collections import defaultdict
from graph_tool.all import Graph, graph_draw, sfdp_layout

# Assign local machine identifiers
mac = get_mac()
home_path = os.environ['HOME']

# Setup up path
year = 2013

if mac == 189262221327399:
    path = home_path + '/OneDrive/Network Centrality/Boardex 2014/Compile/All/Board/' + str(year) + '.gt'
elif mac == 189262223071825:
    path = '/media/esperie/Data/OneDrive/Academia/Research Projects/Network Centrality/Boardex 2014/Compile/All/Board/' + str(year) + '.gt'
elif mac == 105788756394591:
    path = home_path + '/OneDrive/Academia/Research Projects/Network Centrality/Boardex 2014/Compile/All/Board/' + str(year) + '.gt'
elif mac == 203308018878304:
    path = home_path + '/OneDrive/Academia/Research Projects/Network Centrality/Boardex 2014/Compile/All/Board/' + str(year) + '.gt'
else:
    raise Exception('This is not a LINUX machine!')

# load graph
g = gt.load_graph(path)

# load internal property maps
g.list_properties()  # list available property maps
id = g.vertex_properties['director_id']
eigenvector = g.vertex_properties['eigenvector']
vertex_between = g.vertex_properties['vertex_between']
edge_between = g.edge_properties['edge_between']
closeness = g.vertex_properties['closeness']
authority = g.vertex_properties['authority']
hub = g.vertex_properties['hub']

# draw graph
pos = sfdp_layout(g)
graph_draw(g, pos, output_size=(1000, 1000), vertex_color=[1, 1, 1, 0],
           vertex_fill_color=eigenvector, vertex_size=1, edge_pen_width=1.2,
           vcmap=matplotlib.cm.gist_heat_r, output='2013.png')

# components and cluster analysis
comp, hist = gt.topology.label_components(g)

components = defaultdict(int)
for v in g.vertices():
    components[comp[v]] += 1

total_components = len(components)
total_nodes = reduce(lambda x, y: x + y, [int(value) for value in components.values()])  # 93106
rank = sorted([(key, value) for key, value in components.iteritems()], key=lambda x: -x[1])
rank_percent = [(key, float(value)/total_nodes) for key, value in rank]  # first component 85%

# Extract the first component
main_comp = g.new_vertex_property('bool')
for v in g.vertices():
    if comp[v] == 0:
        main_comp[v] = 1
    else:
        main_comp[v] = 0

g.vertex_properties['main_comp'] = main_comp
g.set_vertex_filter(main_comp)
graph_draw(g, pos, output_size=(10000, 10000), vertex_color=[1, 1, 1, 0],
           vertex_fill_color=eigenvector, vertex_size=1, output='2013_first_component.png')

############################################## Extract into dataset (Listed and non-listed Board centrality) ###############################################
# Setup up path
year = 2013

if mac == 189262221327399:
    path = home_path + '/Dropbox/Network Centrality/Boardex 2014/Compile/All/Board/' + str(year) + '.gt'
elif mac == 189262223071825:
    path = '/media/esperie/Data/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/Compile/All/Board/' + str(year) + '.gt'
elif mac == 105788756394591:
    path = home_path + '/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/Compile/All/Board/' + str(year) + '.gt'
else:
    raise Exception('This is not a LINUX machine!')

dirpath, filename = os.path.split(path)
fileslist = []
for root, dirs, files in os.walk(dirpath):
    for file in files:
        if file.endswith('.gt'):
            fileslist.append((dirpath + '/' + file))

results = []

for p in fileslist:
    g = gt.load_graph(p)
    year = int(p.rsplit('/', 1)[-1].replace('.gt', ''))
    num_nodes = g.num_vertices()
    num_edges = g.num_edges()
    id = g.vertex_properties['director_id']
    eigenvector = g.vertex_properties['eigenvector']
    vertex_between = g.vertex_properties['vertex_between']
    closeness = g.vertex_properties['closeness']
    authority = g.vertex_properties['authority']
    hub = g.vertex_properties['hub']

    for v in g.vertices():
        results.append({
            'directorID': id[v],
            'eigenvector': eigenvector[v],
            'vertex_betweenness': vertex_between[v],
            'closeness': closeness[v],
            'authority': authority[v],
            'hub': hub[v],
            'degree': v.out_degree(),
            'degree_norm': float(v.out_degree())/num_nodes,
            'year': year
        })

df = pd.DataFrame(results)

# Export into database
## MongoDB
from network import database
"""
client, db = database.database_setup('boardex', 'localhost')

for r in results:
    db['centrality_board'].insert(r) """

## SQL (send to AWS server)

con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

# con = dbapi.connect(host='esperieaws.cfavanw4rs7k.ap-southeast-1.rds.amazonaws.com', user='Esperie', passwd='momoshU1979', db='network', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS centrality_board_all')
    cur.execute('CREATE TABLE centrality_board_all(directorID BIGINT, year INT, degree BIGINT, degree_norm DOUBLE, eigenvector DOUBLE, betweenness DOUBLE, closeness DOUBLE, authority DOUBLE, hub DOUBLE)')

    for r in results:
        cur.execute('INSERT INTO centrality_board_all VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)', (r['directorID'],
        r['year'], r['degree'], r['degree_norm'], r['eigenvector'], r['vertex_betweenness'], r['closeness'],
        r['authority'], r['hub']))


################################################### Extract into dataset (listed board centrality) ##############################################
# Setup up path
year = 2013

if mac == 189262221327399:
    path = home_path + '/Dropbox/Network Centrality/Boardex 2014/Compile/Listed/Board/' + str(year) + '.gt'
elif mac == 189262223071825:
    path = '/media/esperie/Data/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/Compile/Listed/Board/' + str(year) + '.gt'
elif mac == 105788756394591:
    path = home_path + '/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/Compile/Listed/Board/' + str(year) + '.gt'
else:
    raise Exception('This is not a LINUX machine!')

dirpath, filename = os.path.split(path)
fileslist = []
for root, dirs, files in os.walk(dirpath):
    for file in files:
        if file.endswith('.gt'):
            fileslist.append((dirpath + '/' + file))

results = []

for p in fileslist:
    g = gt.load_graph(p)
    year = int(p.rsplit('/', 1)[-1].replace('.gt', ''))
    num_nodes = g.num_vertices()
    num_edges = g.num_edges()
    id = g.vertex_properties['director_id']
    eigenvector = g.vertex_properties['eigenvector']
    vertex_between = g.vertex_properties['vertex_between']
    closeness = g.vertex_properties['closeness']
    authority = g.vertex_properties['authority']
    hub = g.vertex_properties['hub']

    for v in g.vertices():
        results.append({
            'directorID': id[v],
            'eigenvector': eigenvector[v],
            'vertex_betweenness': vertex_between[v],
            'closeness': closeness[v],
            'authority': authority[v],
            'hub': hub[v],
            'degree': v.out_degree(),
            'degree_norm': float(v.out_degree())/num_nodes,
            'year': year
        })

df = pd.DataFrame(results)

## SQL (send to AWS server)
con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS centrality_board_listed')
    cur.execute('CREATE TABLE centrality_board_listed(directorID BIGINT, year INT, degree BIGINT, degree_norm DOUBLE, eigenvector DOUBLE, betweenness DOUBLE, closeness DOUBLE, authority DOUBLE, hub DOUBLE)')

    for r in results:
        cur.execute('INSERT INTO centrality_board_listed VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)', (r['directorID'],
        r['year'], r['degree'], r['degree_norm'], r['eigenvector'], r['vertex_betweenness'], r['closeness'],
        r['authority'], r['hub']))


################################################### Extract into dataset (listed all centrality) ##############################################
# Setup up path
year = 2013

if mac == 189262221327399:
    path = home_path + '/Dropbox/Network Centrality/Boardex 2014/Compile/Listed/All/' + str(year) + '.gt'
elif mac == 189262223071825:
    path = '/media/esperie/Data/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/Compile/Listed/All/' + str(year) + '.gt'
elif mac == 105788756394591:
    path = home_path + '/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/Compile/Listed/All/' + str(year) + '.gt'
else:
    raise Exception('This is not a LINUX machine!')

dirpath, filename = os.path.split(path)
fileslist = []
for root, dirs, files in os.walk(dirpath):
    for file in files:
        if file.endswith('.gt'):
            fileslist.append((dirpath + '/' + file))

results = []

for p in fileslist:
    g = gt.load_graph(p)
    year = int(p.rsplit('/', 1)[-1].replace('.gt', ''))
    num_nodes = g.num_vertices()
    num_edges = g.num_edges()
    id = g.vertex_properties['director_id']
    eigenvector = g.vertex_properties['eigenvector']
    vertex_between = g.vertex_properties['vertex_between']
    closeness = g.vertex_properties['closeness']
    authority = g.vertex_properties['authority']
    hub = g.vertex_properties['hub']

    for v in g.vertices():
        results.append({
            'directorID': id[v],
            'eigenvector': eigenvector[v],
            'vertex_betweenness': vertex_between[v],
            'closeness': closeness[v],
            'authority': authority[v],
            'hub': hub[v],
            'degree': v.out_degree(),
            'degree_norm': float(v.out_degree())/num_nodes,
            'year': year
        })

df = pd.DataFrame(results)

## SQL (send to AWS server)
con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS centrality_all_listed')
    cur.execute('CREATE TABLE centrality_all_listed(directorID BIGINT, year INT, degree BIGINT, degree_norm DOUBLE, eigenvector DOUBLE, betweenness DOUBLE, closeness DOUBLE, authority DOUBLE, hub DOUBLE)')

    for r in results:
        cur.execute('INSERT INTO centrality_all_listed VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)', (r['directorID'],
        r['year'], r['degree'], r['degree_norm'], r['eigenvector'], r['vertex_betweenness'], r['closeness'],
        r['authority'], r['hub']))