import graph_tool as gt
import datetime
from graph_tool.all import Graph, graph_draw
from network import database

# Check if openMP is open (increases speed by a factor of 3)
gt.show_config()

""" Setup """
client, db = database.database_setup('boardex', 'localhost')
client, db = database.database_setup('boardex', '10.2.20.2')
client, db = database.database_setup('boardex', '192.168.1.109')
database.database_login(client, 'admin')

# Reinitialize DirectorID
main_entities = set(db['director_network'].distinct('DirectorID'))
sub_entities = set(db['director_network'].distinct('Linked DirectorID'))
total_entities = sorted(list(main_entities.union(sub_entities)))
ids = [(i, total_entities[i]) for i in xrange(len(total_entities))]

for i, id in ids:
    db['director_id'].insert(
        {'_id': i,
         'DirectorID': id}
    )

# Update mongodb
for (i, id) in ids:
    db['director_network'].update(
        {'DirectorID': id},
        {'$set': {'p1_ID': i}},
        multi=True
    )

for (i, id) in ids:
    db['director_network'].update(
        {'Linked DirectorID': id},
        {'$set': {'p2_ID': i}},
        multi=True
    )

# Prepare for parsing
years = [y for y in reversed(range(2000, 2002))]

for year in years:
    path = '/home/esperie/Boardex 2014/graph_tools/' + str(year) + '.gt'

    max_vertex = db.director_id.count()
    # Extract director edges from MongoDB
    now = datetime.datetime.now()
    print 'Setting cursor on MongoDB query now... (%s)' % str(now)
    start_time = datetime.datetime.now()
    cur = db['director_network'].find({
        'Beginning of OverLap': {'$lte': datetime.datetime.strptime('31 Dec ' + str(year), '%d %b %Y')},
        'End of OverLap': {'$gte': datetime.datetime.strptime('1 Jan ' + str(year), '%d %b %Y')}
    }, {'_id': 0, 'p1_ID': 1, 'p2_ID': 1})

    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)

    # Remove duplicates (b, a) == (a, b)
    # Compute maximum number of pairs and vertices
    now = datetime.datetime.now()
    print 'Computing pairs and vertices now... (%s)' % str(now)
    start_time = datetime.datetime.now()
    vertices = set()
    pairs = set()
    for c in cur:
        p1 = c['p1_ID']
        p2 = c['p2_ID']
        vertices.add(p1)
        vertices.add(p2)
        if p1 > p2:
            pairs.add((p1, p2))
        else:
            pairs.add((p2, p1))

    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)
    total_vertices = len(vertices)
    total_pairs = len(pairs)
    print 'Total number of vertices: %d.' % total_vertices
    print 'Total number of pairs: %d.' % total_pairs

    # Create graph and initialize vertices
    now = datetime.datetime.now()
    print 'Creating Graph and adding vertices now... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g = Graph(directed=False)

    g.add_vertex(max_vertex)
    for s, t in pairs:
        g.add_edge(g.vertex(s), g.vertex(t))
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)

    # Save graph
    now = datetime.datetime.now()
    print 'Saving graph to gt format... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)

    ## Optional: Load graph
    #g = load_graph(path)

    # Compute measures and save to graph as internal property maps
    now = datetime.datetime.now()
    print 'Calculating Eigenvector centrality... (%s)' % str(now)
    start_time = datetime.datetime.now()
    eigen_max, eigen_map = gt.centrality.eigenvector(g)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)
    now = datetime.datetime.now()
    print 'Creating internal property map and saving graph to gt format... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g.vertex_properties['eigenvector'] = eigen_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)

    now = datetime.datetime.now()
    print 'Calculating Betweenness centrality... (%s)' % str(now)
    start_time = datetime.datetime.now()
    vertex_between, edge_between = gt.centrality.betweenness(g)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)
    now = datetime.datetime.now()
    print 'Creating internal property map and saving graph to gt format... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g.vertex_properties['vertex_between'] = vertex_between
    g.edge_properties['edge_between'] = edge_between
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)

    now = datetime.datetime.now()
    print 'Calculating Closeness centrality... (%s)' % str(now)
    start_time = datetime.datetime.now()
    close_map = gt.centrality.closeness(g)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)
    now = datetime.datetime.now()
    print 'Creating internal property map and saving graph to gt format... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g.vertex_properties['closeness'] = close_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)

    now = datetime.datetime.now()
    print 'Calculating Hits centrality... (%s)' % str(now)
    start_time = datetime.datetime.now()
    hits_max, authority_map, hub_map = gt.centrality.hits(g)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)
    now = datetime.datetime.now()
    print 'Creating internal property map and saving graph to gt format... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g.vertex_properties['authority'] = authority_map
    g.vertex_properties['hub'] = hub_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)

    # load internal property maps
    #g.list_properties()  # list available property maps
    #eigenvector = g.vertex_properties['eigenvector']
    #vertex_between = g.vertex_properties['vertex_between']
    #edge_between = g.edge_properties['edge_between']
    #closeness = g.vertex_properties['close']

"""
    now = datetime.datetime.now()
    print 'Uploading into MongoDB... (%s)' % str(now)
    start_time = datetime.datetime.now()
    for vertex in g.vertices():
        db['centrality'].update(
            {'pID': int(vertex.__str__())},
            {'$set': {str(year) + '.eigenvector': eigen_map[vertex],
                      str(year) + '.vbetweenness': vertex_between[vertex],
                      str(year) + '.ebetweenness': edge_between[vertex],
                      str(year) + '.closeness': close_map[vertex],
                      str(year) + '.hits_max': hits_max[vertex],
                      str(year) + '.authority': authority_map[vertex],
                      str(year) + '.hub': hub_map[vertex]
                      }},
            upsert=True
        )
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken) """
