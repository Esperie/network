from network import database

""" Setup """
client, db = database.database_setup('boardex', 'localhost')
client, db = database.database_setup('boardex', '192.168.1.109')
database.database_login(client, 'admin')

years = [y for y in reversed(range(1980, 2014))]  # year 2013 has been manually completed during checking phase

# Extract director edges from MongoDB

for year in years:
    cur = db['director_network'].find({
        'Beginning of OverLap': {'$lte': datetime.datetime.strptime('31 Dec ' + str(year), '%d %b %Y')},
        'End of OverLap': {'$gte': datetime.datetime.strptime('1 Jan ' + str(year), '%d %b %Y')}
    }, {'_id': 0, 'DirectorID': 1, 'Linked DirectorID': 1})

    # Add edges to networkx
    G = nx.Graph()
    for c in cur:
        G.add_edge(c['DirectorID'], c['Linked DirectorID'])

    path = 'E:/Boardex 2014/networkx/' + str(year)
    nx.write_gpickle(G, path)
    print "Graph of %d successfully pickled (%s)" % (year, path)

    ## optional: load pickle
    # G = nx.read_gpickle(path)

    print 'Number of nodes for Graph of %d: %d.' % (year, G.number_of_nodes())  # Check dimension of graph

    # eigenvector centrality
    now = datetime.datetime.now()
    print 'Eigenvector centrality calculation starts now (%s)' % str(now)
    eigen = nx.eigenvector_centrality(G)
    time_cal = datetime.datetime.now() - now
    print 'Eigenvector centrality for %d calculated.' % year
    print 'Time taken: (%s)' % str(time_cal)
    for key, value in eigen.items():
        db['centrality'].update(
            {'_id': key},
            {'$set': {str(year)+'.eigenvector': value}},
            upsert=True
        )
    time_db = datetime.datetime.now() - now - time_cal
    print 'Successfully entered into database.'
    print 'Time taken: (%s)' % str(time_db)

    # degree centrality
    now = datetime.datetime.now()
    print 'Degree centrality calculation starts now (%s)' % str(now)
    degree = nx.degree_centrality(G)
    time_cal = datetime.datetime.now() - now
    print 'Degree centrality for %d calculated.' % year
    print 'Time taken: (%s)' % str(time_cal)
    for key, value in degree.items():
        db['centrality'].update(
            {'_id': key},
            {'$set': {str(year)+'.degree': value}},
            upsert=True
        )
    time_db = datetime.datetime.now() - now - time_cal
    print 'Successfully entered into database.'
    print 'Time taken: (%s)' % str(time_db)

    # closeness centrality
    now = datetime.datetime.now()
    print 'Closeness centrality calculation starts now (%s)' % str(now)
    close = nx.closeness_centrality(G)
    time_cal = datetime.datetime.now() - now
    print 'Closeness centrality for %d calculated.' % year
    print 'Time taken: (%s)' % str(time_cal)
    for key, value in close.items():
        db['centrality'].update(
            {'_id': str(key)},
            {'$set': {str(year)+'.closeness': value}},
            upsert=True
        )
    time_db = datetime.datetime.now() - now - time_cal
    print 'Successfully entered into database.'
    print 'Time taken: (%s)' % str(time_db)

    # betweenness centrality
    now = datetime.datetime.now()
    print 'Betweenness centrality calculation starts now (%s)' % str(now)
    between = nx.betweenness_centrality(G)
    time_cal = datetime.datetime.now() - now
    print 'Betweenness centrality for %d calculated.' % year
    print 'Time taken: (%s)' % str(time_cal)
    for key, value in between.items():
        db['centrality'].update(
            {'_id': str(key)},
            {'$set': {str(year)+'.betweenness': value}},
            upsert=True
        )
    time_db = datetime.datetime.now() - now - time_cal
    print 'Successfully entered into database.'
    print 'Time taken: (%s)' % str(time_db)

# Testing: Time taken when size increases by factor of 10
##   100: 0:00:00.021230
##  1000: 0:01:10.618638 (factor of 2855.329)
## 10000:
import random
import datetime

for size in (100, 1000, 10000):
    result = [(i, j) for i in range(size) for j in range(size) if random.randint(0, 1) == 1]
    G = nx.Graph()
    print 'Graph object initialized.'
    for r in result:
        G.add_edge(r[0], r[1])
    print 'Graph edges added.'
    start_time = datetime.datetime.now()
    close = nx.closeness_centrality(G)
    time_taken = datetime.datetime.now() - start_time
    print 'Total time taken for %d entities: %s' % (size, str(time_taken))

# Export centrality measure to SQL
# Dataset structure: DirectorID Year eigen degree
import MySQLdb as dbapi
from pandas import DataFrame
con = dbapi.connect(host='localhost', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

cur = db.centrality.find(timeout=False).sort('_id', 1)

for c in cur:
    results = []
    for k in c.keys():
        if k != '_id':
            result = dict()
            result['DirectorID'] = c['_id']
            result['year'] = k
            result['eigen'] = c[k]['eigenvector']
            result['degree'] = c[k]['degree']
        results.append(result)
    df = DataFrame(results, columns=['DirectorID', 'year', 'degree', 'eigen'])
    df.to_sql(con=con, name='centrality', if_exists='append', flavor='mysql')