import os
import socket
from uuid import getnode as get_mac
import datetime
import MySQLdb as dbapi
import pandas as pd
import graph_tool as gt
from graph_tool.all import Graph

# Assign local machine identifiers
mac = get_mac()
home_path = os.environ['HOME']

# Check if openMP is open (increases speed by a factor of 3)
gt.show_config()

# Setup MySQL credentials
def mysql_setup():
    if socket.gethostname() in ['ResearchRoom13', 'esperie-AW'] or socket.gethostbyname(socket.gethostname()).startswith('10.'):
        # return dbapi.connect(host='10.2.8.21', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)
        return dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)
    elif socket.gethostbyname(socket.gethostname()).startswith('192.'):
        return dbapi.connect(host='192.168.1.109', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)
    else:
        return dbapi.connect(host='localhost', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

con = mysql_setup()
df = pd.read_sql("SELECT directorid, linked_directorid, start_year, end_year "
                 "FROM pairs_board", con)

# Prepare for parsing
years = [y for y in reversed(range(2008, 2014))]

for year in years:

    # Set path
    if mac == 189262221327399:
        path = '/home/triojent/Dropbox/Network Centrality/Boardex 2014/Board/' + str(year) + '.gt'
    elif mac == 189262223071825:
        path = '/media/esperie/Data/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/Board/' + str(year) + '.gt'
    elif mac == 105788756394591:
        path = '/home/esperie/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/Board/' + str(year) + '.gt'
    else:
        raise Exception('This is not a LINUX machine!')

    # Extract director pairs from Pandas DataFrame
    df_select = df[df['start_year'] <= year]
    df_select = df_select[df_select['end_year'] >= year].loc[:, ['directorid', 'linked_directorid']].astype(int).values.tolist()

    df_id = set()
    for c0, c1 in df_select:
        df_id.add(c0)
        df_id.add(c1)
    df_id = sorted(list(df_id))

    id_list = {}
    for i in xrange(len(df_id)):
        id_list[df_id[i]] = i

    # Create graph and initialize vertices
    now = datetime.datetime.now()
    print 'Creating Graph and adding vertices now... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g = Graph(directed=False)

    g.add_vertex(len(id_list.keys()))
    id = g.new_vertex_property('int64_t')
    for s, t in df_select:
        g.add_edge(g.vertex(id_list[s]), g.vertex(id_list[t]))
        id[g.vertex(id_list[s])] = s
        id[g.vertex(id_list[t])] = t
    g.vertex_properties['director_id'] = id
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)

    # Save graph
    now = datetime.datetime.now()
    print 'Saving graph to gt format... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)

    ## Optional: Load graph
    #g = load_graph(path)

    # Compute measures and save to graph as internal property maps
    now = datetime.datetime.now()
    print 'Calculating Eigenvector centrality... (%s)' % str(now)
    start_time = datetime.datetime.now()
    eigen_max, eigen_map = gt.centrality.eigenvector(g)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)
    now = datetime.datetime.now()
    print 'Creating internal property map and saving graph to gt format... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g.vertex_properties['eigenvector'] = eigen_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)

    now = datetime.datetime.now()
    print 'Calculating Betweenness centrality... (%s)' % str(now)
    start_time = datetime.datetime.now()
    vertex_between, edge_between = gt.centrality.betweenness(g)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)
    now = datetime.datetime.now()
    print 'Creating internal property map and saving graph to gt format... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g.vertex_properties['vertex_between'] = vertex_between
    g.edge_properties['edge_between'] = edge_between
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)

    now = datetime.datetime.now()
    print 'Calculating Closeness centrality... (%s)' % str(now)
    start_time = datetime.datetime.now()
    close_map = gt.centrality.closeness(g)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)
    now = datetime.datetime.now()
    print 'Creating internal property map and saving graph to gt format... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g.vertex_properties['closeness'] = close_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)

    now = datetime.datetime.now()
    print 'Calculating Hits centrality... (%s)' % str(now)
    start_time = datetime.datetime.now()
    hits_max, authority_map, hub_map = gt.centrality.hits(g)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)
    now = datetime.datetime.now()
    print 'Creating internal property map and saving graph to gt format... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g.vertex_properties['authority'] = authority_map
    g.vertex_properties['hub'] = hub_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)

    # load internal property maps
    #g.list_properties()  # list available property maps
    #eigenvector = g.vertex_properties['eigenvector']
    #vertex_between = g.vertex_properties['vertex_between']
    #edge_between = g.edge_properties['edge_between']
    #closeness = g.vertex_properties['close']

"""
    now = datetime.datetime.now()
    print 'Uploading into MongoDB... (%s)' % str(now)
    start_time = datetime.datetime.now()
    for vertex in g.vertices():
        db['centrality'].update(
            {'pID': int(vertex.__str__())},
            {'$set': {str(year) + '.eigenvector': eigen_map[vertex],
                      str(year) + '.vbetweenness': vertex_between[vertex],
                      str(year) + '.ebetweenness': edge_between[vertex],
                      str(year) + '.closeness': close_map[vertex],
                      str(year) + '.hits_max': hits_max[vertex],
                      str(year) + '.authority': authority_map[vertex],
                      str(year) + '.hub': hub_map[vertex]
                      }},
            upsert=True
        )
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken) """