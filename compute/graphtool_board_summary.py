import os
import socket
from colorama import Fore
import datetime
import MySQLdb as dbapi
import pandas as pd
import graph_tool as gt
from graph_tool.all import Graph

################################################ Graph Tool ##########################################################
# Check if openMP is open (increases speed by a factor of 3)
gt.show_config()

# Setup MySQL credentials
def boardex_setup():
    if socket.gethostname() in ['ResearchRoom13', 'esperie-AW'] or socket.gethostbyname(socket.gethostname()).startswith('10.'):
        # return dbapi.connect(host='10.2.8.21', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)
        return dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)
    elif socket.gethostbyname(socket.gethostname()).startswith('192.'):
        return dbapi.connect(host='192.168.1.109', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)
    else:
        return dbapi.connect(host='localhost', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

def crsp_setup():
    if socket.gethostname() in ['ResearchRoom13', 'esperie-AW'] or socket.gethostbyname(socket.gethostname()).startswith('10.'):
        # return dbapi.connect(host='10.2.8.21', user='Esperie', passwd='momoshU1979', db='crsp', charset='utf8', use_unicode=True)
        return dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='crsp', charset='utf8', use_unicode=True)
    elif socket.gethostbyname(socket.gethostname()).startswith('192.'):
        return dbapi.connect(host='192.168.1.109', user='Esperie', passwd='momoshU1979', db='crsp', charset='utf8', use_unicode=True)
    else:
        return dbapi.connect(host='localhost', user='Esperie', passwd='momoshU1979', db='crsp', charset='utf8', use_unicode=True)

con1 = boardex_setup()
df = pd.read_sql("SELECT directorid, linked_directorid, report_date "
                 "FROM board_summary_network", con1)

# Add year to dataframe
df['year'] = pd.Series([yr.year for yr in df.report_date])


con2 = crsp_setup()
date_id = pd.read_sql("SELECT * FROM date_id", con2)

# Prepare for parsing
year_range = range(2000, 2008)

for year in reversed(year_range):

    # Set path
    if mac == 189262221327399:
        path = '/home/triojent/Dropbox/Network Centrality/Boardex 2014/Summary/' + str(year) + '.gt'
    elif mac == 189262223071825:
        path = '/media/esperie/Data/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/Summary/' + str(year) + '.gt'
    elif mac == 105788756394591:
        path = '/home/esperie/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/Summary/' + str(year) + '.gt'
    else:
        raise Exception('This is not a LINUX machine!')

    # Extract director pairs from Pandas DataFrame
    df_select = df[df.year == year].loc[:, ['directorid', 'linked_directorid']].drop_duplicates().astype(int).values.tolist()

    # Set the pairs to obtain unique ids
    df_id = set()
    for c0, c1 in df_select:
        df_id.add(c0)
        df_id.add(c1)
    df_id = sorted(list(df_id))

    # Create dictionary mapping of ids to index
    id_list = {}
    for i in xrange(len(df_id)):
        id_list[df_id[i]] = i

     # Create graph and initialize variables
    now = datetime.datetime.now()
    print(Fore.CYAN + "Creating graph and adding vertices now... (%s)" % now)
    g = Graph(directed=False)

    g.add_vertex(len(id_list.keys()))
    id = g.new_vertex_property('int64_t')
    for s, t in df_select:
        g.add_edge(g.vertex(id_list[s]), g.vertex(id_list[t]))
        id[g.vertex(id_list[s])] = s
        id[g.vertex(id_list[t])] = t
    g.vertex_properties['directorid'] = id

    time_taken = datetime.datetime.now() - now
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))
    print(Fore.BLUE + 'Year: %d.' % year)
    print(Fore.BLUE + 'Number of vertices: %d.' % g.num_vertices())
    print(Fore.BLUE + 'Number of edges: %d.\n' % g.num_edges())

    # Save graph
    now = datetime.datetime.now()
    print 'Saving graph to gt format... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))

    ## Optional: Load graph
    #g = load_graph(path)

    # Compute measures and save to graph as internal property maps
    now = datetime.datetime.now()
    print(Fore.CYAN + 'Calculating Eigenvector centrality... (%s)' % str(now))
    start_time = datetime.datetime.now()
    eigen_max, eigen_map = gt.centrality.eigenvector(g)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.' % str(time_taken))
    now = datetime.datetime.now()
    print(Fore.CYAN + 'Creating internal property map and saving graph to gt format... (%s)' % str(now))
    start_time = datetime.datetime.now()
    g.vertex_properties['eigenvector'] = eigen_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))

    now = datetime.datetime.now()
    print(Fore.CYAN + 'Calculating Betweenness centrality... (%s)' % str(now))
    start_time = datetime.datetime.now()
    vertex_between, edge_between = gt.centrality.betweenness(g)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.' % str(time_taken))
    now = datetime.datetime.now()
    print (Fore.CYAN + 'Creating internal property map and saving graph to gt format... (%s)' % str(now))
    start_time = datetime.datetime.now()
    g.vertex_properties['vertex_between'] = vertex_between
    g.edge_properties['edge_between'] = edge_between
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))

    now = datetime.datetime.now()
    print(Fore.CYAN + 'Calculating Closeness centrality... (%s)' % str(now))
    start_time = datetime.datetime.now()
    close_map = gt.centrality.closeness(g)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.' % str(time_taken))
    now = datetime.datetime.now()
    print(Fore.CYAN + 'Creating internal property map and saving graph to gt format... (%s)' % str(now))
    start_time = datetime.datetime.now()
    g.vertex_properties['closeness'] = close_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))

    now = datetime.datetime.now()
    print(Fore.CYAN + 'Calculating Hits centrality... (%s)' % str(now))
    start_time = datetime.datetime.now()
    hits_max, authority_map, hub_map = gt.centrality.hits(g)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.' % str(time_taken))
    now = datetime.datetime.now()
    print(Fore.CYAN + 'Creating internal property map and saving graph to gt format... (%s)' % str(now))
    start_time = datetime.datetime.now()
    g.vertex_properties['authority'] = authority_map
    g.vertex_properties['hub'] = hub_map
    g.save(path)
    time_taken = datetime.datetime.now() - start_time
    print(Fore.YELLOW + 'Time taken: %s.\n' % str(time_taken))


################################# Extract into database (summary centrality)

dirpath, filename = os.path.split(path)
fileslist = []
for root, dirs, files in os.walk(dirpath):
    # for dir in dirs:
    for file in files:
        if file.endswith('.gt'):
            fileslist.append((dirpath + '/' + file))

results = []

for p in fileslist:
    g = gt.load_graph(p)
    year = int(p.rsplit('/', 1)[-1].replace('.gt', ''))
    num_nodes = g.num_vertices()
    num_edges = g.num_edges()
    id = g.vertex_properties['directorid']
    eigenvector = g.vertex_properties['eigenvector']
    vertex_between = g.vertex_properties['vertex_between']
    closeness = g.vertex_properties['closeness']
    authority = g.vertex_properties['authority']
    hub = g.vertex_properties['hub']

    for v in g.vertices():
        results.append({
            'directorID': id[v],
            'eigenvector': eigenvector[v],
            'vertex_betweenness': vertex_between[v],
            'closeness': closeness[v],
            'authority': authority[v],
            'hub': hub[v],
            'degree': v.out_degree(),
            'degree_norm': float(v.out_degree())/num_nodes,
            'year': year
        })

## Send to SQL

con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS board_summary_centrality')
    cur.execute('CREATE TABLE board_summary_centrality(directorID BIGINT, year INT, degree BIGINT, degree_norm DOUBLE, eigenvector DOUBLE, betweenness DOUBLE, closeness DOUBLE, authority DOUBLE, hub DOUBLE)')

    for r in results:
        cur.execute('INSERT INTO board_summary_centrality VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)', (r['directorID'],
        r['year'], r['degree'], r['degree_norm'], r['eigenvector'], r['vertex_betweenness'], r['closeness'],
        r['authority'], r['hub']))

