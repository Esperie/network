import os
import socket
from uuid import getnode as get_mac
import datetime
import MySQLdb as dbapi
import pandas as pd
import graph_tool as gt
from graph_tool.all import Graph

# Assign local machine identifiers
mac = get_mac()
home_path = os.environ['HOME']

# Check if openMP is open (increases speed by a factor of 3)
gt.show_config()

# Prepare for parsing

year = 2013
# path = os.environ['HOME'] + '/Boardex 2014/graph_tools/' + str(year) + '.gt'
path = '/home/esperie/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/Board/' + str(year) + '.gt'

# Extract into dataset
dirpath, filename = os.path.split(path)
fileslist = []
for root, dirs, files in os.walk(dirpath):
    for file in files:
        if file.endswith('.gt'):
            fileslist.append((dirpath + '/' + file))

for p in fileslist:
    g = gt.load_graph(p)
    g_test = Graph(g)
    t = Graph(g)
    gt.stats.remove_parallel_edges(t)

    # Compute measures and save to graph as internal property maps
    now = datetime.datetime.now()
    print 'Calculating Eigenvector centrality... (%s)' % str(now)
    start_time = datetime.datetime.now()
    eigen_max, eigen_map = gt.centrality.eigenvector(t)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)
    now = datetime.datetime.now()

    print 'Creating internal property map and saving graph to gt format... (%s)' % str(now)
    start_time = datetime.datetime.now()
    g.vertex_properties['eigenvector_single'] = eigen_map
    g.save(p)
    time_taken = datetime.datetime.now() - start_time
    print 'Time taken: %s.' % str(time_taken)