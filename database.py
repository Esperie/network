import Tkinter
from pymongo import MongoClient

# Ensure correct system default encoding
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

# GUI account authentication
class login_app(Tkinter.Tk):
    """docstring for Values"""
    def __init__(self, master):
        Tkinter.Tk.__init__(self, master)
        self.master = master
        self.initialize()

    def initialize(self):
        self.grid()
        frame = Tkinter.LabelFrame(self, text=" Enter Values: ")
        frame.grid(row=0, columnspan=7, sticky='W', padx=5, pady=5, ipadx=5, ipady=5)
        self.username_label = Tkinter.Label(frame, text="Username: ")
        self.username_label.grid(row=0, column=0, sticky='E', padx=5, pady=2)
        self.username_text = Tkinter.Entry(frame, width=45)
        self.username_text.grid(row=0, column=1, columnspan=3, pady=2, sticky='WE')
        self.password_label = Tkinter.Label(frame, text="Password: ")
        self.password_label.grid(row=1, column=0, sticky='E', padx=5, pady=2)
        self.password_text = Tkinter.Entry(frame, show="*", width=45)
        self.password_text.grid(row=1, column=1, columnspan=3, pady=2, sticky='WE')
        self.error_label = Tkinter.Label(frame, text="", fg="red")
        self.error_label.grid(row=2, column=1, columnspan=3, sticky='E', padx=5, pady=2)

        self.username = None
        self.password = None

        submit_btn = Tkinter.Button(frame, text="Submit", command=self.submit)
        submit_btn.grid(row=3, column=3, sticky='E', padx=5, pady=2)
        self.bind('<Return>', self.submit)

    def submit(self, *args):
        self.username = self.username_text.get()
        self.password = self.password_text.get()

        if self.username and self.password != "":
            self.destroy()
        else:
            self.error_label['text'] = 'Username and password fields cannot be empty.'

def login():
    cred = login_app(None)
    cred.title('Authentication System')
    cred.mainloop()
    username = cred.username
    password = cred.password
    return username, password

# Setup Database
def database_setup(db, host='localhost', port=27017):
    """Setup database links.
    Returns client and database"""
    client = MongoClient(host, port, max_pool_size=None)
    client[db]  # Assign CapitalIQ database
    return client, client[db]

def database_login(client, db):
    username, password = login()
    try:
        client[db].authenticate(username, password)
        print "User [%s] has logged in successfully." % username
    except:
        print "Please authenticate again or logout current user."

