import os
import datetime
from colorama import Fore
from uuid import getnode as get_mac
import MySQLdb as dbapi
import pandas as pd
import numpy as np

# Assign local machine identifiers
mac = get_mac()
home_path = os.environ['HOME']

# Set path
if mac == 189262221327399:
    dirpath = '/media/triojent/Database/Boardex 2014/North America 4 - SMDE Director/Profiles/'
elif mac == 189262223071825:
    dirpath = '/media/esperie/Data/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/North America 4 - SMDE Director/Profiles/'
elif mac == 105788756394591:
    dirpath = '/home/esperie/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/North America 4 - SMDE Director/Profiles/'
else:
    dirpath = 'E:/Boardex 2014/North America 4 - SMDE Director/Profiles/'

################################################ Extract characteristics ###############################################
filepath = "Task 2008 NA - SMDE's Profile - Characteristics.xlsx"
path = dirpath + filepath

print(Fore.YELLOW + 'Working on <Characteristics> tab from (%s) now...\n' % filepath)
df = pd.read_excel(path, 'Characteristics')
df = df.replace(u'n.a', np.nan)  # Replace all 'N.A' with NaN/None/Null
df.loc[df['Annual Report Year'] == 'Current', 'Annual Report Year'] = 2013  # Replace 'Current" in report year to '2013'

dob = []
dob_exact = []
for e in df['DOB']:
    if type(e) == int:
        e = datetime.date(e, 1, 1)
        check = 0
    elif isinstance(e, datetime.datetime):
        e = datetime.date(e.year, e.month, e.day)
        check = 1
    elif e == 'Unknown':
        e = np.nan
        check = 0
    else:
        check = 1
    dob.append(e)
    dob_exact.append(check)

dob = pd.Series(dob)
dob_exact = pd.Series(dob_exact)
df['DOB'] = dob
df['DOB_exact'] = dob_exact

dod = []
dod_exact = []
for e in df['DOD']:
    if type(e) == int:
        e = datetime.date(e, 12, 31)
        check = 0
    elif isinstance(e, datetime.datetime):
        e = datetime.date(e.year, e.month, e.day)
        check = 1
    elif np.isnan(e):
        check = 0
    else:
        check = 1
    dod.append(e)
    dod_exact.append(check)

dod = pd.Series(dod)
dod_exact = pd.Series(dod_exact)
df['DOD'] = dod
df['DOD_exact'] = dod_exact

# df.loc[df['DOD'].isnull(), 'DOD_exact'] = 0
df.rename(columns={'DirectorID*': 'DirectorID'}, inplace=True)

# Change column order
df = df[['DirectorID', 'Age', 'DOB', 'DOB_exact', 'DOD', 'DOD_exact', 'Gender', 'Nationality', 'Annual Report Year',
         'Quoted_todate', 'Private_todate', 'Other_todate', 'Quoted_current', 'Private_current', 'Other_current',
         'Avg Years on Quoted Boards']]
df['smde'] = 1

## Check length
# Director ID : BIGINT
# Age: INT
# DOB: Date
# DOB_exact: BOOLEAN
# DOD: Date
# DOD_exact: BOOLEAN
# Gender: VARCHAR(1)
len(max(df['Nationality'], key=lambda x: len(x)))  # VARCHAR()
# Annual Report Year: INT
# Quoted_todate: INT
# Private_todate: INT
# Other_todate: INT
# Quoted_current: INT
# Private_current: INT
# Other_current: INT
# Avg Years on Quoted Boards: FLOAT
         
con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS profile_smde_characteristics')
    cur.execute('CREATE TABLE profile_smde_characteristics(directorID BIGINT, age INT, dob DATE, dob_exact BOOLEAN, dod DATE, dod_exact BOOLEAN, '
                'gender VARCHAR(1), nationality VARCHAR(23), report_year INT, quoted_to_date INT, private_to_date INT, '
                'others_to_date INT, quoted_current INT, private_current INT, others_current INT, avg_yr_quoted FLOAT, smde BOOLEAN)')

    for entry in df.values.tolist():
        r = []
        for e in entry:
            if isinstance(e, datetime.date) or type(e) == unicode:
                r.append(e)
                continue
            if np.isnan(e):
                e = None
            r.append(e)
        cur.execute('INSERT INTO profile_smde_characteristics VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                    (r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15], r[16]))

################################################## Current Board Role ###############################################
filepath = "Task 2008 NA - SMDE's Profile - Employment.xlsx"
path = dirpath + filepath

print(Fore.YELLOW + 'Working on <Current Board Role> tab from (%s) now...\n' % filepath)
df = pd.read_excel(path, 'Current Board Role')

df.rename(columns={'CountryName': 'Country', 'Format': 'Start Date Format', 'Format.1': 'End Date Format'}, inplace=True)  # rename columns

# Change column order
df = df[['Director ID', 'Country', 'CompanyID', 'Company Name', 'Company ISIN', 'Company Index', 'Sector Name',
         'Company Type', 'Role', 'Role Description', 'ED/NED', 'Start Date', 'End Date', 'Committee Name',
         'Committee Role', 'Start Date Format', 'End Date Format']]

## Check length
# Director ID : BIGINT
s1 = len(max(df[df['Country'].apply(type) == unicode]['Country'], key=lambda x: len(x)))  # VARCHAR()
# CompanyID: BIGINT
s2 = len(max(df[df['Company Name'].apply(type) == unicode]['Company Name'], key=lambda x: len(x)))  # VARCHAR()
s3 = len(max(df[df['Company ISIN'].apply(type) == unicode]['Company ISIN'], key=lambda x: len(x)))  # VARCHAR()
s4 = len(max(df[df['Company Index'].apply(type) == unicode]['Company Index'], key=lambda x: len(x)))  # VARCHAR()
s5 = len(max(df[df['Sector Name'].apply(type) == unicode]['Sector Name'], key=lambda x: len(x)))  # VARCHAR()
s6 = len(max(df[df['Company Type'].apply(type) == unicode]['Company Type'], key=lambda x: len(x)))  # VARCHAR()
s7 = len(max(df[df['Role'].apply(type) == unicode]['Role'], key=lambda x: len(x)))  # VARCHAR()
s8 = len(max(df[df['Role Description'].apply(type) == unicode]['Role Description'], key=lambda x: len(x)))  # VARCHAR()
s9 = len(max(df[df['ED/NED'].apply(type) == unicode]['ED/NED'], key=lambda x: len(x)))  # VARCHAR()
# Start Date: DATE
# End Date: DATE
s10 = len(max(df[df['Committee Name'].apply(type) == unicode]['Committee Name'], key=lambda x: len(x)))  # VARCHAR()
s11 = len(max(df[df['Committee Role'].apply(type) == unicode]['Committee Role'], key=lambda x: len(x)))  # VARCHAR()
s12 = len(max(df[df['Start Date Format'].apply(type) == unicode]['Start Date Format'], key=lambda x: len(x)))  # VARCHAR()
s13 = len(max(df[df['End Date Format'].apply(type) == unicode]['End Date Format'], key=lambda x: len(x)))  # VARCHAR()

con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS profile_smde_current_board')
    cur.execute('CREATE TABLE profile_smde_current_board(directorID BIGINT, country VARCHAR(%d), companyID BIGINT,'
                'conm VARCHAR(%d), isin VARCHAR(%d), idx VARCHAR(%d), sector VARCHAR(%d), type VARCHAR(%d), '
                'role VARCHAR(%d), description VARCHAR(%d), ed_ned_sm VARCHAR(%d), start_date DATE, end_date DATE, '
                'committee VARCHAR(%d),committee_role VARCHAR(%d), sdate_format VARCHAR(%d), edate_format VARCHAR(%d))'
                % (s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13))


    for entry in df.values.tolist():
        r = []
        for e in entry:
            if pd.isnull(e):
                e = None
                r.append(e)
                continue
            if isinstance(e, datetime.date) or type(e) == unicode:
                r.append(e)
                continue
            r.append(e)
        cur.execute('INSERT INTO profile_smde_current_board VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                    (r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15], r[16]))

################################################## Historic Board Role ###############################################
filepath = "Task 2008 NA - SMDE's Profile - Employment.xlsx"
path = dirpath + filepath

print(Fore.YELLOW + 'Working on <Historic Board Role> tab from (%s) now...\n' % filepath)
df = pd.read_excel(path, 'Historic Board Role')

df.rename(columns={'CountryName': 'Country', 'Format': 'Start Date Format', 'Format.1': 'End Date Format'}, inplace=True)  # rename columns

# Change column order
df = df[['Director ID', 'Country', 'CompanyID', 'Company Name', 'Company ISIN', 'Company Index', 'Sector Name',
         'Company Type', 'Role', 'Role Description', 'ED/NED', 'Start Date', 'End Date', 'Committee Name',
         'Committee Role', 'Start Date Format', 'End Date Format']]

## Check length
# Director ID : BIGINT
s1 = len(max(df[df['Country'].apply(type) == unicode]['Country'], key=lambda x: len(x)))  # VARCHAR()
# CompanyID: BIGINT
s2 = len(max(df[df['Company Name'].apply(type) == unicode]['Company Name'], key=lambda x: len(x)))  # VARCHAR()
s3 = len(max(df[df['Company ISIN'].apply(type) == unicode]['Company ISIN'], key=lambda x: len(x)))  # VARCHAR()
s4 = len(max(df[df['Company Index'].apply(type) == unicode]['Company Index'], key=lambda x: len(x)))  # VARCHAR()
s5 = len(max(df[df['Sector Name'].apply(type) == unicode]['Sector Name'], key=lambda x: len(x)))  # VARCHAR()
s6 = len(max(df[df['Company Type'].apply(type) == unicode]['Company Type'], key=lambda x: len(x)))  # VARCHAR()
s7 = len(max(df[df['Role'].apply(type) == unicode]['Role'], key=lambda x: len(x)))  # VARCHAR()
s8 = len(max(df[df['Role Description'].apply(type) == unicode]['Role Description'], key=lambda x: len(x)))  # VARCHAR()
s9 = len(max(df[df['ED/NED'].apply(type) == unicode]['ED/NED'], key=lambda x: len(x)))  # VARCHAR()
# Start Date: DATE
# End Date: DATE
s10 = len(max(df[df['Committee Name'].apply(type) == unicode]['Committee Name'], key=lambda x: len(x)))  # VARCHAR()
s11 = len(max(df[df['Committee Role'].apply(type) == unicode]['Committee Role'], key=lambda x: len(x)))  # VARCHAR()
s12 = len(max(df[df['Start Date Format'].apply(type) == unicode]['Start Date Format'], key=lambda x: len(x)))  # VARCHAR()
s13 = len(max(df[df['End Date Format'].apply(type) == unicode]['End Date Format'], key=lambda x: len(x)))  # VARCHAR()

con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS profile_smde_historic_board')
    cur.execute('CREATE TABLE profile_smde_historic_board(directorID BIGINT, country VARCHAR(%d), companyID BIGINT,'
                'conm VARCHAR(%d), isin VARCHAR(%d), idx VARCHAR(%d), sector VARCHAR(%d), type VARCHAR(%d), '
                'role VARCHAR(%d), description VARCHAR(%d), ed_ned_sm VARCHAR(%d), start_date DATE, end_date DATE, '
                'committee VARCHAR(%d),committee_role VARCHAR(%d), sdate_format VARCHAR(%d), edate_format VARCHAR(%d))'
                % (s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13))

    for entry in df.values.tolist():
        r = []
        for e in entry:
            if pd.isnull(e):
                e = None
                r.append(e)
                continue
            if isinstance(e, datetime.date) or type(e) == unicode:
                r.append(e)
                continue
            r.append(e)
        cur.execute('INSERT INTO profile_smde_historic_board VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                    (r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15], r[16]))

################################################## Current NonBoard Role ###############################################
filepath = "Task 2008 NA - SMDE's Profile - Employment.xlsx"
path = dirpath + filepath

print(Fore.YELLOW + 'Working on <Current Non Board Role> tab from (%s) now...\n' % filepath)
df = pd.read_excel(path, 'Current Non Board Role')

df.rename(columns={'Format': 'Start Date Format', 'Format.1': 'End Date Format'}, inplace=True)  # rename columns

# Change column order
df = df[['Director ID', 'Country', 'CompanyID', 'Company Name', 'Company ISIN', 'Company Index', 'Sector Name',
         'Company Type', 'Role', 'Role Description', 'ED/NED/SM', 'Start Date', 'End Date', 'Committee Name',
         'Committee Role', 'Start Date Format', 'End Date Format']]

## Check length
# Director ID : BIGINT
s1 = len(max(df[df['Country'].apply(type) == unicode]['Country'], key=lambda x: len(x)))  # VARCHAR()
# CompanyID: BIGINT
s2 = len(max(df[df['Company Name'].apply(type) == unicode]['Company Name'], key=lambda x: len(x)))  # VARCHAR()
s3 = len(max(df[df['Company ISIN'].apply(type) == unicode]['Company ISIN'], key=lambda x: len(x)))  # VARCHAR()
s4 = len(max(df[df['Company Index'].apply(type) == unicode]['Company Index'], key=lambda x: len(x)))  # VARCHAR()
s5 = len(max(df[df['Sector Name'].apply(type) == unicode]['Sector Name'], key=lambda x: len(x)))  # VARCHAR()
s6 = len(max(df[df['Company Type'].apply(type) == unicode]['Company Type'], key=lambda x: len(x)))  # VARCHAR()
s7 = len(max(df[df['Role'].apply(type) == unicode]['Role'], key=lambda x: len(x)))  # VARCHAR()
s8 = len(max(df[df['Role Description'].apply(type) == unicode]['Role Description'], key=lambda x: len(x)))  # VARCHAR()
s9 = len(max(df[df['ED/NED/SM'].apply(type) == unicode]['ED/NED/SM'], key=lambda x: len(x)))  # VARCHAR()
# Start Date: DATE
# End Date: DATE
# s10 = len(max(df[df['Committee Name'].apply(type) == unicode]['Committee Name'], key=lambda x: len(x)))  # VARCHAR()
# s11 = len(max(df[df['Committee Role'].apply(type) == unicode]['Committee Role'], key=lambda x: len(x)))  # VARCHAR()
s12 = len(max(df[df['Start Date Format'].apply(type) == unicode]['Start Date Format'], key=lambda x: len(x)))  # VARCHAR()
s13 = len(max(df[df['End Date Format'].apply(type) == unicode]['End Date Format'], key=lambda x: len(x)))  # VARCHAR()

con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS profile_smde_current_nonboard')
    cur.execute('CREATE TABLE profile_smde_current_nonboard(directorID BIGINT, country VARCHAR(%d), companyID BIGINT, '
                'conm VARCHAR(%d), isin VARCHAR(%d), idx VARCHAR(%d), sector VARCHAR(%d), type VARCHAR(%d), '
                'role VARCHAR(%d), description VARCHAR(%d), ed_ned_sm VARCHAR(%d), start_date DATE, end_date DATE, '
                'committee VARCHAR(%d),committee_role VARCHAR(%d), sdate_format VARCHAR(%d), edate_format VARCHAR(%d))'
                % (s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13))

    for entry in df.values.tolist():
        r = []
        for e in entry:
            if pd.isnull(e):
                e = None
                r.append(e)
                continue
            if isinstance(e, datetime.date) or type(e) == unicode:
                r.append(e)
                continue
            r.append(e)
        cur.execute('INSERT INTO profile_smde_current_nonboard VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                    (r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15], r[16]))

################################################## Historic NonBoard Role ###############################################
filepath = "Task 2008 NA - SMDE's Profile - Employment.xlsx"
path = dirpath + filepath

print(Fore.YELLOW + 'Working on <Historic Non Board Role> tab from (%s) now...\n' % filepath)
df = pd.read_excel(path, 'Historic Non Board Role')
df.rename(columns={'Format': 'Start Date Format', 'Format.1': 'End Date Format'}, inplace=True)  # rename columns

# Change column order
df = df[['Director ID', 'Country', 'CompanyID', 'Company Name', 'Company ISIN', 'Company Index', 'Sector Name',
         'Company Type', 'Role', 'Role Description', 'ED/NED/SM', 'Start Date', 'End Date', 'Committee Name',
         'Committee Role', 'Start Date Format', 'End Date Format']]

## Check length
# Director ID : BIGINT
s1 = len(max(df[df['Country'].apply(type) == unicode]['Country'], key=lambda x: len(x)))  # VARCHAR()
# CompanyID: BIGINT
s2 = len(max(df[df['Company Name'].apply(type) == unicode]['Company Name'], key=lambda x: len(x)))  # VARCHAR()
s3 = len(max(df[df['Company ISIN'].apply(type) == unicode]['Company ISIN'], key=lambda x: len(x)))  # VARCHAR()
s4 = len(max(df[df['Company Index'].apply(type) == unicode]['Company Index'], key=lambda x: len(x)))  # VARCHAR()
s5 = len(max(df[df['Sector Name'].apply(type) == unicode]['Sector Name'], key=lambda x: len(x)))  # VARCHAR()
s6 = len(max(df[df['Company Type'].apply(type) == unicode]['Company Type'], key=lambda x: len(x)))  # VARCHAR()
s7 = len(max(df[df['Role'].apply(type) == unicode]['Role'], key=lambda x: len(x)))  # VARCHAR()
s8 = len(max(df[df['Role Description'].apply(type) == unicode]['Role Description'], key=lambda x: len(x)))  # VARCHAR()
s9 = len(max(df[df['ED/NED/SM'].apply(type) == unicode]['ED/NED/SM'], key=lambda x: len(x)))  # VARCHAR()
# Start Date: DATE
# End Date: DATE
# s10 = len(max(df[df['Committee Name'].apply(type) == unicode]['Committee Name'], key=lambda x: len(x)))  # VARCHAR()
# s11 = len(max(df[df['Committee Role'].apply(type) == unicode]['Committee Role'], key=lambda x: len(x)))  # VARCHAR()
s12 = len(max(df[df['Start Date Format'].apply(type) == unicode]['Start Date Format'], key=lambda x: len(x)))  # VARCHAR()
s13 = len(max(df[df['End Date Format'].apply(type) == unicode]['End Date Format'], key=lambda x: len(x)))  # VARCHAR()

con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS profile_smde_historic_nonboard')
    cur.execute('CREATE TABLE profile_smde_historic_nonboard(directorID BIGINT, country VARCHAR(%d), companyID BIGINT, '
                'conm VARCHAR(%d), isin VARCHAR(%d), idx VARCHAR(%d), sector VARCHAR(%d), type VARCHAR(%d), '
                'role VARCHAR(%d), description TEXT, ed_ned_sm VARCHAR(%d), start_date DATE, end_date DATE, '
                'committee VARCHAR(%d),committee_role VARCHAR(%d), sdate_format VARCHAR(%d), edate_format VARCHAR(%d))'
                % (s1, s2, s3, s4, s5, s6, s7, s9, s10, s11, s12, s13))

    for entry in df.values.tolist():
        r = []
        for e in entry:
            if pd.isnull(e):
                e = None
                r.append(e)
                continue
            if isinstance(e, datetime.date) or type(e) == unicode:
                r.append(e)
                continue
            r.append(e)
        cur.execute('INSERT INTO profile_smde_historic_nonboard VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                    (r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15], r[16]))


############################################## Current Other Activities ###############################################
filepath = "Task 2008 NA - SMDE's Profile - Other Activities.xlsx"
path = dirpath + filepath

print(Fore.YELLOW + 'Working on <Current Other Activities> tab from (%s) now...\n' % filepath)
df = pd.read_excel(path, 'Current Other Activities')

df.rename(columns={'Format': 'Start Date Format', 'Format.1': 'End Date Format'}, inplace=True)  # rename columns

# Change column order
df = df[['Director ID', 'Country', 'CompanyID', 'Company Name', 'Role', 'Role Description', 'Start Date', 'End Date',
         'Start Date Format', 'End Date Format']]

## Check length
# Director ID : BIGINT
s1 = len(max(df[df['Country'].apply(type) == unicode]['Country'], key=lambda x: len(x)))  # VARCHAR()
# CompanyID: BIGINT
s2 = len(max(df[df['Company Name'].apply(type) == unicode]['Company Name'], key=lambda x: len(x)))  # VARCHAR()
s3 = len(max(df[df['Role'].apply(type) == unicode]['Role'], key=lambda x: len(x)))  # VARCHAR()
s4 = len(max(df[df['Role Description'].apply(type) == unicode]['Role Description'], key=lambda x: len(x)))  # VARCHAR()
# Start Date: DATE
# End Date: DATE
s5 = len(max(df[df['Start Date Format'].apply(type) == unicode]['Start Date Format'], key=lambda x: len(x)))  # VARCHAR()
s6 = len(max(df[df['End Date Format'].apply(type) == unicode]['End Date Format'], key=lambda x: len(x)))  # VARCHAR()

con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS profile_smde_current_others')
    cur.execute('CREATE TABLE profile_smde_current_others(directorID BIGINT, country VARCHAR(%d), companyID BIGINT, '
                'conm VARCHAR(%d), role VARCHAR(%d), description VARCHAR(%d), start_date DATE, end_date DATE, '
                'sdate_format VARCHAR(%d), edate_format VARCHAR(%d))' % (s1, s2, s3, s4, s5, s6))

    for entry in df.values.tolist():
        r = []
        for e in entry:
            if pd.isnull(e):
                e = None
                r.append(e)
                continue
            if isinstance(e, datetime.date) or type(e) == unicode:
                r.append(e)
                continue
            r.append(e)
        cur.execute('INSERT INTO profile_smde_current_others VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                    (r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9]))


############################################# Historic Other Activities ###############################################
filepath = "Task 2008 NA - SMDE's Profile - Other Activities.xlsx"
path = dirpath + filepath

print(Fore.YELLOW + 'Working on <Historic Other Activities> tab from (%s) now...\n' % filepath)
df = pd.read_excel(path, 'Historic Other Activities')

df.rename(columns={'Format': 'Start Date Format', 'Format.1': 'End Date Format'}, inplace=True)  # rename columns

# Change column order
df = df[['Director ID', 'Country', 'CompanyID', 'Company Name', 'Role', 'Role Description', 'Start Date', 'End Date',
         'Start Date Format', 'End Date Format']]

## Check length
# Director ID : BIGINT
s1 = len(max(df[df['Country'].apply(type) == unicode]['Country'], key=lambda x: len(x)))  # VARCHAR()
# CompanyID: BIGINT
s2 = len(max(df[df['Company Name'].apply(type) == unicode]['Company Name'], key=lambda x: len(x)))  # VARCHAR()
s3 = len(max(df[df['Role'].apply(type) == unicode]['Role'], key=lambda x: len(x)))  # VARCHAR()
s4 = len(max(df[df['Role Description'].apply(type) == unicode]['Role Description'], key=lambda x: len(x)))  # VARCHAR()
# Start Date: DATE
# End Date: DATE
s5 = len(max(df[df['Start Date Format'].apply(type) == unicode]['Start Date Format'], key=lambda x: len(x)))  # VARCHAR()
s6 = len(max(df[df['End Date Format'].apply(type) == unicode]['End Date Format'], key=lambda x: len(x)))  # VARCHAR()

con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS profile_smde_historic_others')
    cur.execute('CREATE TABLE profile_smde_historic_others(directorID BIGINT, country VARCHAR(%d), companyID BIGINT, '
                'conm VARCHAR(%d), role VARCHAR(%d), description VARCHAR(%d), start_date DATE, end_date DATE, '
                'sdate_format VARCHAR(%d), edate_format VARCHAR(%d))' % (s1, s2, s3, s4, s5, s6))

    for entry in df.values.tolist():
        r = []
        for e in entry:
            if pd.isnull(e):
                e = None
                r.append(e)
                continue
            if isinstance(e, datetime.date) or type(e) == unicode:
                r.append(e)
                continue
            r.append(e)
        cur.execute('INSERT INTO profile_smde_historic_others VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                    (r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9]))


################################################## Education ###############################################
filepath = "Task 2008 NA - SMDE's Profile - Education & Achievements.xlsx"
path = dirpath + filepath

print(Fore.YELLOW + 'Working on <Education> tab from (%s) now...\n' % filepath)
df = pd.read_excel(path, 'Education')

df.rename(columns={'Format': 'Qualification Date Format'}, inplace=True)  # rename columns

# Change column order
df = df[['Director ID', 'Country', 'Company ID', 'Institution Name', 'Company Type', 'Qualification',
         'Qualification Description', 'Qualification Date', 'Qualification Date Format']]

## Check length
# Director ID : BIGINT
s1 = len(max(df[df['Country'].apply(type) == unicode]['Country'], key=lambda x: len(x)))  # VARCHAR()
# Company ID: BIGINT
s2 = len(max(df[df['Institution Name'].apply(type) == unicode]['Institution Name'], key=lambda x: len(x)))  # VARCHAR()
s3 = len(max(df[df['Company Type'].apply(type) == unicode]['Company Type'], key=lambda x: len(x)))  # VARCHAR()
s4 = len(max(df[df['Qualification'].apply(type) == unicode]['Qualification'], key=lambda x: len(x)))  # VARCHAR()
s5 = len(max(df[df['Qualification Description'].apply(type) == unicode]['Qualification Description'], key=lambda x: len(x)))  # VARCHAR()
# Qualification Date: DATE
s6 = len(max(df[df['Qualification Date Format'].apply(type) == unicode]['Qualification Date Format'], key=lambda x: len(x)))  # VARCHAR()

con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS profile_smde_education')
    cur.execute('CREATE TABLE profile_smde_education(directorID BIGINT, country VARCHAR(%d), companyID BIGINT, '
                'conm VARCHAR(%d), type VARCHAR(%d), qualification VARCHAR(%d), description VARCHAR(%d), '
                'qualification_date DATE, qdate_format VARCHAR(%d))' % (s1, s2, s3, s4, s5, s6))

    for entry in df.values.tolist():
        r = []
        for e in entry:
            if pd.isnull(e):
                e = None
                r.append(e)
                continue
            if isinstance(e, datetime.date) or type(e) == unicode:
                r.append(e)
                continue
            r.append(e)
        cur.execute('INSERT INTO profile_smde_education VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)',
                    (r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8]))


################################################## Achievements ###############################################
filepath = "Task 2008 NA - SMDE's Profile - Education & Achievements.xlsx"
path = dirpath + filepath

print(Fore.YELLOW + 'Working on <Achievements> tab from (%s) now...\n' % filepath)
df = pd.read_excel(path, 'Achievements')

df.rename(columns={'Format': 'Achievement Date Format'}, inplace=True)  # rename columns

# Change column order
df = df[['Director ID', 'Country', 'OrganisationID', 'OrganisationName', 'Achievement Date', 'Award\Achievement',
         'Achievement Date Format']]

## Check length
# Director ID : BIGINT
s1 = len(max(df[df['Country'].apply(type) == unicode]['Country'], key=lambda x: len(x)))  # VARCHAR()
# OrganisationID: BIGINT
s2 = len(max(df[df['OrganisationName'].apply(type) == unicode]['OrganisationName'], key=lambda x: len(x)))  # VARCHAR()
# Achievement Date: DATE
s3 = len(max(df[df['Award\Achievement'].apply(type) == unicode]['Award\Achievement'], key=lambda x: len(x)))  # VARCHAR()
s4 = len(max(df[df['Achievement Date Format'].apply(type) == unicode]['Achievement Date Format'], key=lambda x: len(x)))  # VARCHAR()

con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS profile_smde_achievement')
    cur.execute('CREATE TABLE profile_smde_achievement(directorID BIGINT, country VARCHAR(%d), companyID BIGINT, '
                'conm VARCHAR(%d), achievement_date DATE, description VARCHAR(%d), adate_format VARCHAR(%d))'
                % (s1, s2, s3, s4))

    for entry in df.values.tolist():
        r = []
        for e in entry:
            if pd.isnull(e):
                e = None
                r.append(e)
                continue
            if isinstance(e, datetime.date) or type(e) == unicode:
                r.append(e)
                continue
            r.append(e)
        cur.execute('INSERT INTO profile_smde_achievement VALUES(%s, %s, %s, %s, %s, %s, %s)',
                    (r[0], r[1], r[2], r[3], r[4], r[5], r[6]))