from sas7bdat import SAS7BDAT
import datetime
import MySQLdb as dbapi

dirpath = "F:/Centrality/"
filename = "connections_pro_final.sas7bdat"

dsiobj = SAS7BDAT(dirpath + filename)  # Create a sas7bdat object

print dsiobj.header  # Check headers

content = dsiobj.readData()  # Create generator object
variables = content.next()
sasDate = datetime.date(1960, 1, 1)

# Setup MySQL server for entry
con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS connections_pro_final')
    cur.execute('CREATE TABLE connections_pro_final(directorID BIGINT, linked_directorid BIGINT, '
                'companyID BIGINT, conm VARCHAR(128), sector VARCHAR(39), type VARCHAR(12), p1_class VARCHAR(3), '
                'p2_class VARCHAR(3), p1_role VARCHAR(75), p1_role_description VARCHAR(239), p2_role VARCHAR(75), '
                'p2_role_description VARCHAR(239), p1_start_date DATE, p1_end_date DATE, p2_start_date DATE, '
                'p2_end_date DATE, p1_committee VARCHAR(61), p1_committee_role VARCHAR(39), p2_committee VARCHAR(61), '
                'p2_committee_role VARCHAR(39), overlap_start DATE, overlap_end DATE,'
                'start_trading_day BOOLEAN, start_date_id INT, start_month_id INT, start_calqtr INT,'
                'start_quarter_id INT, end_trading_day BOOLEAN, end_date_id INT, end_month_id INT,'
                'end_calqtr INT, end_quarter_id INT)')

    for c in content:
        i = 0
        entry = []
        for e in c:
            if i in (3, 4, 5, 6, 7, 8, 9, 10, 11, 16, 17, 18, 19):
                try:
                    string = unicode(e, 'utf-8')
                except UnicodeDecodeError:
                    string = unicode(e, 'cp1252')
                entry.append(string)
            elif i in (12, 13, 14, 15, 20, 21):
                date = datetime.datetime.strptime(e, '%d%b%Y').date()
                entry.append(date)
            else:
                entry.append(e)
            i += 1
        cur.execute('INSERT INTO connections_pro_final VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, '
                    '%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                    (entry[0], entry[1], entry[2], entry[3], entry[4], entry[5], entry[6], entry[7], entry[8], entry[9],
                     entry[10], entry[11], entry[12], entry[13], entry[14], entry[15], entry[16], entry[17], entry[18],
                     entry[19], entry[20], entry[21], entry[22], entry[23], entry[24], entry[25], entry[26], entry[27],
                     entry[28], entry[29], entry[30], entry[31]))

dirpath = "F:/Centrality/"
filename = "connections_soc_final.sas7bdat"

dsiobj = SAS7BDAT(dirpath + filename)  # Create a sas7bdat object

print dsiobj.header  # Check headers

content = dsiobj.readData()  # Create generator object
variables = content.next()
sasDate = datetime.date(1960, 1, 1)

# Setup MySQL server for entry
con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS connections_soc_final')
    cur.execute('CREATE TABLE connections_soc_final(directorID BIGINT, linked_directorid BIGINT, companyID BIGINT, '
                'conm VARCHAR(128), sector VARCHAR(39), type VARCHAR(12), p1_class VARCHAR(3), p2_class VARCHAR(3), '
                'p1_role VARCHAR(75), p1_role_description VARCHAR(239), p2_role VARCHAR(75), '
                'p2_role_description VARCHAR(239), p1_start_date DATE, p1_end_date DATE, p2_start_date DATE, '
                'p2_end_date DATE, p1_committee VARCHAR(61), p1_committee_role VARCHAR(39), p2_committee VARCHAR(61), '
                'p2_committee_role VARCHAR(39))')

    for c in content:
        i = 0
        entry = []
        for e in c:
            if i in (3, 4, 5, 6, 7, 8, 9, 10, 11, 16, 17, 18, 19):
                try:
                    string = unicode(e, 'utf-8')
                except UnicodeDecodeError:
                    string = unicode(e, 'cp1252')
                entry.append(string)
            elif i in (12, 13, 14, 15):
                if e:
                    date = datetime.datetime.strptime(e, '%d%b%Y').date()
                    entry.append(date)
                else:
                    entry.append(None)
            else:
                entry.append(e)
            i += 1

        cur.execute('INSERT INTO connections_soc_final VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, '
                    '%s, %s, %s, %s, %s, %s)',
                    (entry[0], entry[1], entry[2], entry[3], entry[4], entry[5], entry[6], entry[7], entry[8], entry[9],
                     entry[10], entry[11], entry[12], entry[13], entry[14], entry[15], entry[16], entry[17], entry[18],
                     entry[19]))

dirpath = "F:/Centrality/"
filename = "connections_edu.sas7bdat"

dsiobj = SAS7BDAT(dirpath + filename)  # Create a sas7bdat object

print dsiobj.header  # Check headers

content = dsiobj.readData()  # Create generator object
variables = content.next()
sasDate = datetime.date(1960, 1, 1)

# Setup MySQL server for entry
con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS connections_soc_final')
    cur.execute('CREATE TABLE connections_soc_final(directorID BIGINT, linked_directorid BIGINT, companyID BIGINT, '
                'conm VARCHAR(128), sector VARCHAR(39), type VARCHAR(12), p1_class VARCHAR(3), p2_class VARCHAR(3), '
                'p1_role VARCHAR(75), p1_role_description VARCHAR(239), p2_role VARCHAR(75), '
                'p2_role_description VARCHAR(239), p1_start_date DATE, p1_end_date DATE, p2_start_date DATE, '
                'p2_end_date DATE, p1_committee VARCHAR(61), p1_committee_role VARCHAR(39), p2_committee VARCHAR(61), '
                'p2_committee_role VARCHAR(39))')

    for c in content:
        i = 0
        entry = []
        for e in c:
            if i in (3, 4, 5, 6, 7, 8, 9, 10, 11, 16, 17, 18, 19):
                try:
                    string = unicode(e, 'utf-8')
                except UnicodeDecodeError:
                    string = unicode(e, 'cp1252')
                entry.append(string)
            elif i in (12, 13, 14, 15):
                if e:
                    date = datetime.datetime.strptime(e, '%d%b%Y').date()
                    entry.append(date)
                else:
                    entry.append(None)
            else:
                entry.append(e)
            i += 1

        cur.execute('INSERT INTO connections_soc_final VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, '
                    '%s, %s, %s, %s, %s, %s)',
                    (entry[0], entry[1], entry[2], entry[3], entry[4], entry[5], entry[6], entry[7], entry[8], entry[9],
                     entry[10], entry[11], entry[12], entry[13], entry[14], entry[15], entry[16], entry[17], entry[18],
                     entry[19]))