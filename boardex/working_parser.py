import os
import re
import socket
import math
import xlrd
from datetime import datetime
from network import database
from network.boardex import parser
import pandas as pd
from pandas import Series, DataFrame
import MySQLdb as dbapi

""" Setup """
# Director directory
dirpath = parser.check_path1()
files_list = parser.files_list(dirpath)

files_list = [f for f in files_list if f.startswith('Task') and f.endswith('.xlsx')]

# SMDE DIrector directory
dirpath = parser.check_path2()
files_list = parser.files_list(dirpath)

files_list = [f for f in files_list if f.startswith('Task') and f.endswith('.xlsx')]


# Database setup
client, db = database.database_setup('boardex', 'localhost')
client, db = database.database_setup('boardex', '192.168.1.109')
database.database_login(client, 'admin')

title_rows = 1
count = 0

# Parse excel files and enter results into MongoDB
for file in files_list:
    workbook = xlrd.open_workbook(dirpath + file)
    worksheet = workbook.sheet_by_name('Director Network')
    num_rows = worksheet.nrows
    num_cols = worksheet.ncols
    title = [title.value.replace('*', '') for title in worksheet.row(title_rows - 1)]
    title[10:] = ['P2 Title', 'P2 ED/NED/SM', 'P1 Title', 'P1 ED/NED/SM']
    title.insert(0, '_id')
    #print title

    result = []
    for row in range(title_rows, num_rows):
        result.append([count])
        count += 1
        for col in range(num_cols):
            cell = worksheet.cell(row, col)
            if col == 8 or col == 9:
                if type(cell.value) == float:
                    if cell.value < 2015:
                        if col == 8:
                            t = datetime(*(int(cell.value), 1, 1))
                        else:
                            t = datetime(*(int(cell.value), 12, 31))
                    else:
                        t = datetime(*(xlrd.xldate_as_tuple(cell.value, 0)))
                elif re.match(u'current', cell.value, re.IGNORECASE):
                    t = datetime(*(2013, 12, 31))
                else:
                    t = cell.value
                result[row-title_rows].append(t)
                continue

            if cell.ctype == 2:
                result[row-title_rows].append(int(cell.value))
            else:
                result[row-title_rows].append(cell.value)

    for r in result:
        db['director_network'].insert(
            dict(zip(title, r)))

# Extract from MongoDB and enter into MySQL
cur = db.director_network.find({}, {'_id': 0}, timeout=False)
cur = db.smde_network.find({}, {'_id': 0}, timeout=False)
# Check length of text variables

def choose_max(a, b):
    result = []
    for i in range(len(a)):
        if a[i] > b[i]:
            result.append(a[i])
        else:
            result.append(b[i])
    return result

initial_list = [0, 0, 0, 0, 0, 0, 0, 0, 0]
for c in cur:
    new_list = [len(c['P1 Title']), len(c['P1 ED/NED/SM']), len(c['P2 Title']), len(c['P2 ED/NED/SM']), len(c['Connected Company']), len(c['Connected Company Type']), len(c['Index']), len(c['Sector']), len(c['Date of overlap'])]
    initial_list = choose_max(initial_list, new_list)

# Merge Director and SMDE datasets

## Compute all possible DirectorIDs in the datasets
from collections import defaultdict

directorIDs = set()
mongocur = db.director_network.find({}, {'_id': 0, 'DirectorID': 1, 'Linked DirectorID': 1}, timeout=False)

for c in mongocur:
    directorIDs.add(c['DirectorID'])
    directorIDs.add(c['Linked DirectorID'])

mongocur = db.smde_network.find({}, {'_id': 0, 'DirectorID': 1, 'Linked DirectorID': 1}, timeout=False)

for c in mongocur:
    directorIDs.add(c['DirectorID'])
    directorIDs.add(c['Linked DirectorID'])

con = dbapi.connect(host='localhost', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

## Assign directorIDs

dlist = sorted(list(directorIDs))
directors_id = dict([(dlist[i], i) for i in xrange(len(dlist))])

for entry in dlist:
    db.directors_id.insert({
        '_id': directors_id[entry],
        'DirectorID': entry
    })

## Populate into MySQL
# Connect to MySQL database

try:
    if socket.gethostbyname(socket.gethostname()).startswith('10.'):
        con = dbapi.connect(host='10.2.8.21', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)
    elif socket.gethostbyname(socket.gethostname()).startswith('192.'):
        con = dbapi.connect(host='192.168.1.109', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)
except:
    con = dbapi.connect(host='localhost', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS director_network')
    cur.execute('CREATE TABLE director_network(directorID BIGINT, p1_id INT, p1_title VARCHAR(85), p1_ed_ned_sm VARCHAR(7), linked_directorID BIGINT, p2_id INT, p2_title VARCHAR(85), p2_ed_ned_sm VARCHAR(7), connected_companyID BIGINT, connected_company VARCHAR(128), connected_company_type VARCHAR(12), idx VARCHAR(106), sector VARCHAR(39), date_overlap VARCHAR(42), start_overlap DATE, end_overlap DATE, smde BOOLEAN)')

    mongocur = db.director_network.find({}, {'_id': 0}, timeout=False)
    smde = False

    for c in mongocur:
        if c['DirectorID'] < c['Linked DirectorID']:
            dID = c['DirectorID']
            p1_id = directors_id[dID]
            p1_title = c['P1 Title']
            p1_ed_ned_sm = c['P1 ED/NED/SM']

            lID = c['Linked DirectorID']
            p2_id = directors_id[lID]
            p2_title = c['P2 Title']
            p2_ed_ned_sm = c['P2 ED/NED/SM']
        else:
            dID = c['Linked DirectorID']
            p1_id = directors_id[dID]
            p1_title = c['P2 Title']
            p1_ed_ned_sm = c['P2 ED/NED/SM']

            lID = c['DirectorID']
            p2_id = directors_id[lID]
            p2_title = c['P1 Title']
            p2_ed_ned_sm = c['P1 ED/NED/SM']

        cID = c['Connected CompanyID']
        conm = c['Connected Company']
        ctype = c['Connected Company Type']
        idx = c['Index']
        sector = c['Sector']
        date_overlap = c['Date of overlap']
        start_overlap = c['Beginning of OverLap']
        end_overlap = c['End of OverLap']

        cur.execute('INSERT INTO director_network VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)', (dID, p1_id, p1_title, p1_ed_ned_sm, lID, p2_id, p2_title, p2_ed_ned_sm, cID, conm, ctype, idx, sector, date_overlap, start_overlap, end_overlap, smde))

    mongocur = db.smde_network.find({}, {'_id': 0}, timeout=False)
    smde = True

    for c in mongocur:
        if c['DirectorID'] < c['Linked DirectorID']:
            dID = c['DirectorID']
            p1_id = directors_id[dID]
            p1_title = c['P1 Title']
            p1_ed_ned_sm = c['P1 ED/NED/SM']

            lID = c['Linked DirectorID']
            p2_id = directors_id[lID]
            p2_title = c['P2 Title']
            p2_ed_ned_sm = c['P2 ED/NED/SM']
        else:
            dID = c['Linked DirectorID']
            p1_id = directors_id[dID]
            p1_title = c['P2 Title']
            p1_ed_ned_sm = c['P2 ED/NED/SM']

            lID = c['DirectorID']
            p2_id = directors_id[lID]
            p2_title = c['P1 Title']
            p2_ed_ned_sm = c['P1 ED/NED/SM']

        cID = c['Connected CompanyID']
        conm = c['Connected Company']
        ctype = c['Connected Company Type']
        idx = c['Index']
        sector = c['Sector']
        date_overlap = c['Date of overlap']
        start_overlap = c['Beginning of OverLap']
        end_overlap = c['End of OverLap']

        cur.execute('INSERT INTO director_network VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)', (dID, p1_id, p1_title, p1_ed_ned_sm, lID, p2_id, p2_title, p2_ed_ned_sm, cID, conm, ctype, idx, sector, date_overlap, start_overlap, end_overlap, smde))

# Save company details from excel to MySQL
co_det = pd.read_excel('E:/Boardex 2014/North America 1/Task 1878 NA - Company Details.xlsx', 'Company Details', index_col=None, na_values=None)
co_det.columns = ['CompanyID', 'conm', 'hqadd1', 'hqadd2', 'hqadd3', 'hqadd4', 'hqadd5', 'home_country', 'tel', 'fax', 'url', 'ccadd1', 'ccadd2', 'ccadd3', 'ccadd4', 'ccadd5', 'cc_country', 'cc_tel', 'cc_fax', 'cik', 'isin', 'ticker', 'sector', 'idx', 'mktcap', 'revenue_year_end', 'latest_ar', 'auditors', 'bankers']

co_select = co_det.loc[:, ['CompanyID', 'conm', 'home_country', 'cik', 'isin', 'ticker']]

## Check for max length
for title in co_select.columns:
    print title, co_select[title].dtype.__str__()
    try:
        print len(max(co_select[title], key=lambda x: len(x)))
    except TypeError:
        continue

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS co_details')
    cur.execute('CREATE TABLE co_details(CompanyID BIGINT, conm VARCHAR(128), home_country VARCHAR(24), cik VARCHAR(10), isin VARCHAR(264), ticker VARCHAR(70))')

    for c in co_select.values.tolist():
        CompanyID = c[0]
        conm = c[1]
        home_country = c[2]
        cik = c[3]
        if type(cik) == float and math.isnan(cik):
            cik = None
        else:
            cik = str(int(cik))
            if len(cik) < 10:
                cik = '0' * (10 - len(cik)) + cik
        isin = c[4]
        if type(isin) == float:
            isin = None
        ticker = c[5]
        if type(ticker) == float:
            ticker = None
        cur.execute('INSERT INTO co_details VALUES(%s, %s, %s, %s, %s, %s)', (CompanyID, conm, home_country, cik, isin, ticker))


co_select.to_sql(con=con, name='co_details', if_exists='replace', flavor='mysql')

# Save director mapping file to MySQL
df_map = pd.read_excel('E:/Boardex 2014/North America 2 - Director/Profiles/Task 2008 NA - Director Details Mapping File.xlsx', 'Sheet1', index_col=None, na_values=None)

df_map.columns = ['id', 'directorID', 'director_name']

df_map.to_sql(con=con, name='id_map', if_exists='replace', flavor='mysql')

# Save director characteristics file to MySQL
df_char = pd.read_excel('E:/Boardex 2014/North America 2 - Director/Profiles/Task 2008 NA - Director Profile - Characteristics.xlsx', 'Characteristics', index_col=None, na_values=None)

df_char.columns = ['directorID', 'age', 'dob', 'dod', 'gender', 'nationality', 'annual_report_year', 'listed_boards_todate', 'private_boards_todate', 'other_boards_todate', 'listed_boards_2013', 'private_boards_2013', 'other_boards_2013', 'years_on_listed_boards']

df_char.to_sql(con=con, name='characteristics', if_exists='replace', flavor='mysql', chunksize=10000)

# Extract employment information
cur = db.director_network.find()

for c in cur:
    start = c['Beginning of OverLap'].year
    end = c['End of OverLap'].year
    for i in range(start, end + 1):
        db.firm.insert({
            'year': i,
            'DirectorID': c['DirectorID'],
            'pID': c['p1_ID'],
            'Title': c['P1 Title'],
            'ED-NED-SM': c['P1 ED/NED/SM'],
            'Firm': c['Connected Company'],
            'Type': c['Connected Company Type'],
            'CompanyID': c['Connected CompanyID'],
            'Index': c['Index'],
            'Sector': c['Sector']
        })
        db.firm.insert({
            'year': i,
            'DirectorID': c['Linked DirectorID'],
            'pID': c['p2_ID'],
            'Title': c['P2 Title'],
            'ED-NED-SM': c['P2 ED/NED/SM'],
            'Firm': c['Connected Company'],
            'Type': c['Connected Company Type'],
            'CompanyID': c['Connected CompanyID'],
            'Index': c['Index'],
            'Sector': c['Sector']
        })
