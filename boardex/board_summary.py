import os
from uuid import getnode as get_mac
from dateutil.relativedelta import relativedelta
import datetime
import MySQLdb as dbapi
import pandas as pd

########################################## Boardex Board Summary ######################################################

# Assign local machine identifiers
mac = get_mac()
home_path = os.environ['HOME']

# Set path
if mac == 189262221327399:
    dirpath = '/home/triojent/Dropbox/Network Centrality/Boardex 2014/'
elif mac == 189262223071825:
    dirpath = '/media/esperie/Data/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/'
elif mac == 105788756394591:
    dirpath = '/home/esperie/Dropbox/Academia/Research Projects/Network Centrality/Boardex 2014/'
else:
    dirpath = 'E:/Boardex 2014/'

# Extract board summary
filepath = 'North America 1/Task 1878 NA - Board Summary - 1.xlsx'
path = dirpath + filepath

df_bsum1 = pd.read_excel(path, 'Board Summary ')

filepath = 'North America 1/Task 1878 NA - Board Summary - 2.xlsx'
path = dirpath + filepath

df_bsum2 = pd.read_excel(path, 'Board Summary ')

## Check columns
df_bsum2.columns = df_bsum1.columns

## append
df_bsum = df_bsum1.append(df_bsum2, ignore_index=True)

df_bsum.columns = ['annual_report_year', 'country', 'sector', 'conm', 'coID', 'isin', 'ticker', 'idx', 'ed_sd',
                   'name', 'maskedID', 'role', 'network_size', 'time_to_retire', 'time_in_role', 'time_on_board',
                   'time_in_company', 'totalnum_quoted_to_date', 'totalnum_pte_to_date', 'totalnum_others_to_date',
                   'totalnum_quoted_current', 'totalnum_pte_current', 'totalnum_others_current', 'avg_years_on_other_quoted',
                   'age', 'num_qualifications', 'gender', 'nationality', 'bonus_percent', 'equity_percent',
                   'performance_percent', 'change_from_last', 'wealth_delta', 'total_directors', 'total_sd', 'total_ind_ned',
                   'duality', 'ind_directors_finance_background', 'salary', 'bonus', 'pension', 'other', 'total_salary_bonus',
                   'total_incl_pension', 'shares', 'ltips', 'vesting_date', 'intrinsic_opt_exercisable',
                   'intrinsic_opt_unexercisable', 'estimated_opt_exercisable', 'estimated_opt_unexercisable', 'share_price',
                   'total_equity_linked_compensation', 'total_annual_compensation', 'shares1', 'ltips1', 'intrinsic_opt',
                   'estimated_opt', 'liquid_wealth', 'total_wealth', 'audit_comm_member', 'remuneration_comm_member',
                   'nomination_comm_member', 'audit_comm_size', 'remuneration_comm_size', 'nomination_comm_size',
                   'num_ind_NED_audit', 'num_ind_NED_remuneration', 'num_ind_NED_nomination', 'num_ind_NED_audit_excl_chairman',
                   'num_ind_NED_remuneration_excl_chairman', 'num_ind_NED_nomination_excl_chairman',
                   'num_ind_NED_functional_expertise_audit']

df_select = df_bsum.iloc[:, :12]

ary = []
for year in df_select.annual_report_year:
    if year == 'Current':
        ary.append(datetime.date(2013, 12, 31))
    else:
        year = year + relativedelta(months=1) - relativedelta(days=1)
        ary.append(datetime.date(year.year, year.month, year.day))

df_select.annual_report_year = ary

for title in ['country', 'sector', 'conm', 'isin', 'ticker', 'idx', 'ed_sd', 'name', 'role']:
    if title in ['isin', 'ticker', 'idx']:
        print len(max(df_select[df_select[title].notnull()][title], key=lambda x: len(x)))
    else:
        print len(max(df_select[title], key=lambda x: len(x)))

con = dbapi.connect(host='10.2.20.2', user='Esperie', passwd='momoshU1979', db='boardex', charset='utf8', use_unicode=True)

with con:
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS board_summary')
    cur.execute('CREATE TABLE board_summary(annual_report_year DATE, country VARCHAR(24), sector VARCHAR(39), '
                'conm VARCHAR(128), coID BIGINT, isin VARCHAR(264), ticker VARCHAR(70), idx VARCHAR(87), '
                'ed_sd VARCHAR(2), directorname VARCHAR(62), maskedID BIGINT, role VARCHAR(75))')

    for entry in df_select.values.tolist():
        r = []
        for e in entry:
            if isinstance(e, datetime.date) or type(e) == unicode:
                r.append(e)
                continue
            if np.isnan(e):
                e = None
            r.append(e)
        cur.execute('INSERT INTO board_summary VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                    (r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11]))

df_bsum.to_sql(con=con, name='board_summary', if_exists='replace', flavor='mysql')