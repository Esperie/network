import os
from colorama import Fore, Back, Style  # colorama has CMYK, RGB, reset

def check_path1():
    """Checks OS and assigns path.
    Returns directory path."""
    if os.name == 'nt':
        dirpath = 'E:/Boardex 2014/North America 2 - Director/'
    elif os.name == 'posix':
        try:
            os.environ['CINNAMON_VERSION']
            dirpath = '/media/esperie/Database/Boardex 2014/North America 2 - Director/'
        except KeyError:
            dirpath = '/Volumes/Database/Boardex 2014/North America 2 - Director/'
    else:
        print(Fore.RED + 'Not Windows, Mac or Linux OS' + Fore.RESET)
        return None
    if os.path.exists(dirpath):
        print(Fore.GREEN + "Directory path found: %s" % dirpath + Fore.RESET)
        return dirpath
    print(Fore.Red + "No such directory path. Please perform a manual check." + Fore.RESET)


def check_path2():
    """Checks OS and assigns path.
    Returns directory path."""
    if os.name == 'nt':
        dirpath = 'E:/Boardex 2014/North America 4 - SMDE Director/'
    elif os.name == 'posix':
        try:
            os.environ['CINNAMON_VERSION']
            dirpath = '/media/esperie/Database/Boardex 2014/North America 4 - SMDE Director/'
        except KeyError:
            dirpath = '/Volumes/Database/Boardex 2014/North America 4 - SMDE Director/'
    else:
        print(Fore.RED + 'Not Windows, Mac or Linux OS' + Fore.RESET)
        return None
    if os.path.exists(dirpath):
        print(Fore.GREEN + "Directory path found: %s" % dirpath + Fore.RESET)
        return dirpath
    print(Fore.Red + "No such directory path. Please perform a manual check." + Fore.RESET)


def files_list(dirpath):
    return [files for root, dir, files in os.walk(dirpath)][0]